<?php

namespace KURZ\VisitNet\PublicService\Arrays;

/**
 * This class stands for ArrayOfGuestData Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfGuestData
 * @subpackage Arrays
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class ArrayOfGuestData
{
    /**
     * The GuestData
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @validate NotEmpty
     * @var \KURZ\VisitNet\PublicService\Structs\GuestData[]
     * @validate \KURZ\KurzGaesteanmeldung\Validator\GuestDataValidator
     */
    public $GuestData;
    /**
     * Constructor method for ArrayOfGuestData
     * @uses ArrayOfGuestData::setGuestData()
     * @param \KURZ\VisitNet\PublicService\Structs\GuestData[] $guestData
     */
    public function __construct(array $guestData = array())
    {
        $this
            ->setGuestData($guestData);
    }
    /**
     * Get GuestData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\GuestData[]|null
     */
    public function getGuestData()
    {
        return isset($this->GuestData) ? $this->GuestData : null;
    }
    /**
     * Set GuestData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\GuestData[] $guestData
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData
     */
    public function setGuestData(array $guestData = array())
    {
        if (is_null($guestData) || (is_array($guestData) && empty($guestData))) {
            unset($this->GuestData);
        } else {
            $this->GuestData = $guestData;
        }
        return $this;
    }
    /**
     * Add item to GuestData value
     * @throws \InvalidArgumentException
     * @param \KURZ\VisitNet\PublicService\Structs\GuestData $item
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData
     */
    public function addToGuestData(\KURZ\VisitNet\PublicService\Structs\GuestData $item)
    {
        $this->GuestData[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see ::current()
     * @return \KURZ\VisitNet\PublicService\Structs\GuestData|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see ::item()
     * @param int $index
     * @return \KURZ\VisitNet\PublicService\Structs\GuestData|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see ::first()
     * @return \KURZ\VisitNet\PublicService\Structs\GuestData|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see ::last()
     * @return \KURZ\VisitNet\PublicService\Structs\GuestData|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see ::offsetGet()
     * @param int $offset
     * @return \KURZ\VisitNet\PublicService\Structs\GuestData|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see ::getAttributeName()
     * @return string GuestData
     */
    public function getAttributeName()
    {
        return 'GuestData';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
