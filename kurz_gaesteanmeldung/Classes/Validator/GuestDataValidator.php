<?php

namespace KURZ\KurzGaesteanmeldung\Validator;


use TYPO3\CMS\Extbase\Validation\Validator\GenericObjectValidator;
use TYPO3\CMS\Extbase\Validation\Validator\ValidatorInterface;


class GuestDataValidator extends GenericObjectValidator implements ValidatorInterface
{

    /**
     * @var \TYPO3\CMS\Extbase\Validation\ValidatorResolver
     * @inject
     */
    protected $validatorResolver;


    /**
     * @var \TYPO3\CMS\Extbase\Error\Result
     * @inject
     */
    protected $result;


    /**
     * Checks if the given value is valid according to the validator, and returns
     * the Error Messages object which occurred.
     *
     * @param mixed $value The value that should be validated
     * @return \TYPO3\CMS\Extbase\Error\Result
     */
    public function validate($array)
    {
        $value = $array[0];
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        // Create an Instance of a ValidatorResolver Object
        $this->validatorResolver = $objectManager->get('TYPO3\CMS\Extbase\Validation\ValidatorResolver');

        // Create an Instance of a ErrorResult Object
        $this->result = $objectManager->get('TYPO3\CMS\Extbase\Error\Result');

        //foreach ($array as $value) {

            if ($this->acceptsEmptyValues === false || $this->isEmpty($value) === false) {
                if (!is_object($value)) {
                    $this->addError('Object expected, %1$s given.', 1241099149, [gettype($value)]);
                } elseif ($this->isValidatedAlready($value) === false) {
                    # Create an add propertyValidators
                    #---------------------------------
                    $this->addPropertyValidator('FirstName', $this->validatorResolver->createValidator('NotEmpty'));
                    $this->addPropertyValidator('FirstName', $this->validatorResolver->createValidator('StringLength', array('minimum' => 2)));
                    //$this->addPropertyValidator('EMailAddress', $this->validatorResolver->createValidator('NotEmpty'));
                   // $this->addPropertyValidator('EMailAddress', $this->validatorResolver->createValidator('EmailAddress'));
                    $this->addPropertyValidator('LastName', $this->validatorResolver->createValidator('NotEmpty'));
                    $this->addPropertyValidator('LastName', $this->validatorResolver->createValidator('StringLength', array('minimum' => 2)));
                    $this->isValid($value);
                }
            }
        //}
        return $this->result;
    }

}