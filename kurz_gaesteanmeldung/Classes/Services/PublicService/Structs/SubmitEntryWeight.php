<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for SubmitEntryWeight Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class SubmitEntryWeight
{
    /**
     * The visitId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $visitId;
    /**
     * The weighingIdentity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $weighingIdentity;
    /**
     * The weight
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $weight;
    /**
     * The weigher
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $weigher;
    /**
     * Constructor method for SubmitEntryWeight
     * @uses SubmitEntryWeight::setVisitId()
     * @uses SubmitEntryWeight::setWeighingIdentity()
     * @uses SubmitEntryWeight::setWeight()
     * @uses SubmitEntryWeight::setWeigher()
     * @param int $visitId
     * @param int $weighingIdentity
     * @param int $weight
     * @param string $weigher
     */
    public function __construct($visitId = null, $weighingIdentity = null, $weight = null, $weigher = null)
    {
        $this
            ->setVisitId($visitId)
            ->setWeighingIdentity($weighingIdentity)
            ->setWeight($weight)
            ->setWeigher($weigher);
    }
    /**
     * Get visitId value
     * @return int|null
     */
    public function getVisitId()
    {
        return $this->visitId;
    }
    /**
     * Set visitId value
     * @param int $visitId
     * @return \KURZ\VisitNet\PublicService\Structs\SubmitEntryWeight
     */
    public function setVisitId($visitId = null)
    {
        $this->visitId = $visitId;
        return $this;
    }
    /**
     * Get weighingIdentity value
     * @return int|null
     */
    public function getWeighingIdentity()
    {
        return $this->weighingIdentity;
    }
    /**
     * Set weighingIdentity value
     * @param int $weighingIdentity
     * @return \KURZ\VisitNet\PublicService\Structs\SubmitEntryWeight
     */
    public function setWeighingIdentity($weighingIdentity = null)
    {
        $this->weighingIdentity = $weighingIdentity;
        return $this;
    }
    /**
     * Get weight value
     * @return int|null
     */
    public function getWeight()
    {
        return $this->weight;
    }
    /**
     * Set weight value
     * @param int $weight
     * @return \KURZ\VisitNet\PublicService\Structs\SubmitEntryWeight
     */
    public function setWeight($weight = null)
    {
        $this->weight = $weight;
        return $this;
    }
    /**
     * Get weigher value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getWeigher()
    {
        return isset($this->weigher) ? $this->weigher : null;
    }
    /**
     * Set weigher value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $weigher
     * @return \KURZ\VisitNet\PublicService\Structs\SubmitEntryWeight
     */
    public function setWeigher($weigher = null)
    {
        if (is_null($weigher) || (is_array($weigher) && empty($weigher))) {
            unset($this->weigher);
        } else {
            $this->weigher = $weigher;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\SubmitEntryWeight
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
