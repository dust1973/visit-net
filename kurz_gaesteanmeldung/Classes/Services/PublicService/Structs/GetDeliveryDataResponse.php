<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for GetDeliveryDataResponse Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class GetDeliveryDataResponse
{
    /**
     * The GetDeliveryDataResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\DeliveryData
     */
    public $GetDeliveryDataResult;
    /**
     * Constructor method for GetDeliveryDataResponse
     * @uses GetDeliveryDataResponse::setGetDeliveryDataResult()
     * @param \KURZ\VisitNet\PublicService\Structs\DeliveryData $getDeliveryDataResult
     */
    public function __construct(\KURZ\VisitNet\PublicService\Structs\DeliveryData $getDeliveryDataResult = null)
    {
        $this
            ->setGetDeliveryDataResult($getDeliveryDataResult);
    }
    /**
     * Get GetDeliveryDataResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\DeliveryData|null
     */
    public function getGetDeliveryDataResult()
    {
        return isset($this->GetDeliveryDataResult) ? $this->GetDeliveryDataResult : null;
    }
    /**
     * Set GetDeliveryDataResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\DeliveryData $getDeliveryDataResult
     * @return \KURZ\VisitNet\PublicService\Structs\GetDeliveryDataResponse
     */
    public function setGetDeliveryDataResult(\KURZ\VisitNet\PublicService\Structs\DeliveryData $getDeliveryDataResult = null)
    {
        if (is_null($getDeliveryDataResult) || (is_array($getDeliveryDataResult) && empty($getDeliveryDataResult))) {
            unset($this->GetDeliveryDataResult);
        } else {
            $this->GetDeliveryDataResult = $getDeliveryDataResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\GetDeliveryDataResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
