<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for AnsweredQuestionMultiSelectData Structs
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:AnsweredQuestionMultiSelectData
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class AnsweredQuestionMultiSelectData extends ExtendedAnsweredQuestionData
{
    /**
     * The AnswerDatas
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnswerData
     */
    public $AnswerDatas;
    /**
     * Constructor method for AnsweredQuestionMultiSelectData
     * @uses AnsweredQuestionMultiSelectData::setAnswerDatas()
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnswerData $answerDatas
     */
    public function __construct(\KURZ\VisitNet\PublicService\Arrays\ArrayOfAnswerData $answerDatas = null)
    {
        $this
            ->setAnswerDatas($answerDatas);
    }
    /**
     * Get AnswerDatas value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnswerData|null
     */
    public function getAnswerDatas()
    {
        return isset($this->AnswerDatas) ? $this->AnswerDatas : null;
    }
    /**
     * Set AnswerDatas value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnswerData $answerDatas
     * @return \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionMultiSelectData
     */
    public function setAnswerDatas(\KURZ\VisitNet\PublicService\Arrays\ArrayOfAnswerData $answerDatas = null)
    {
        if (is_null($answerDatas) || (is_array($answerDatas) && empty($answerDatas))) {
            unset($this->AnswerDatas);
        } else {
            $this->AnswerDatas = $answerDatas;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionMultiSelectData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
