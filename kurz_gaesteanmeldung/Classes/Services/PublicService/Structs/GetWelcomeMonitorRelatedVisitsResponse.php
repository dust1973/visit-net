<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for GetWelcomeMonitorRelatedVisitsResponse Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class GetWelcomeMonitorRelatedVisitsResponse
{
    /**
     * The GetWelcomeMonitorRelatedVisitsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData
     */
    public $GetWelcomeMonitorRelatedVisitsResult;
    /**
     * Constructor method for GetWelcomeMonitorRelatedVisitsResponse
     * @uses GetWelcomeMonitorRelatedVisitsResponse::setGetWelcomeMonitorRelatedVisitsResult()
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getWelcomeMonitorRelatedVisitsResult
     */
    public function __construct(\KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getWelcomeMonitorRelatedVisitsResult = null)
    {
        $this
            ->setGetWelcomeMonitorRelatedVisitsResult($getWelcomeMonitorRelatedVisitsResult);
    }
    /**
     * Get GetWelcomeMonitorRelatedVisitsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData|null
     */
    public function getGetWelcomeMonitorRelatedVisitsResult()
    {
        return isset($this->GetWelcomeMonitorRelatedVisitsResult) ? $this->GetWelcomeMonitorRelatedVisitsResult : null;
    }
    /**
     * Set GetWelcomeMonitorRelatedVisitsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getWelcomeMonitorRelatedVisitsResult
     * @return \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisitsResponse
     */
    public function setGetWelcomeMonitorRelatedVisitsResult(\KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getWelcomeMonitorRelatedVisitsResult = null)
    {
        if (is_null($getWelcomeMonitorRelatedVisitsResult) || (is_array($getWelcomeMonitorRelatedVisitsResult) && empty($getWelcomeMonitorRelatedVisitsResult))) {
            unset($this->GetWelcomeMonitorRelatedVisitsResult);
        } else {
            $this->GetWelcomeMonitorRelatedVisitsResult = $getWelcomeMonitorRelatedVisitsResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisitsResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
