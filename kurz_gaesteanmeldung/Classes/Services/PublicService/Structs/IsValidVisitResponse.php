<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for IsValidVisitResponse Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class IsValidVisitResponse
{
    /**
     * The IsValidVisitResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $IsValidVisitResult;
    /**
     * Constructor method for IsValidVisitResponse
     * @uses IsValidVisitResponse::setIsValidVisitResult()
     * @param bool $isValidVisitResult
     */
    public function __construct($isValidVisitResult = null)
    {
        $this
            ->setIsValidVisitResult($isValidVisitResult);
    }
    /**
     * Get IsValidVisitResult value
     * @return bool|null
     */
    public function getIsValidVisitResult()
    {
        return $this->IsValidVisitResult;
    }
    /**
     * Set IsValidVisitResult value
     * @param bool $isValidVisitResult
     * @return \KURZ\VisitNet\PublicService\Structs\IsValidVisitResponse
     */
    public function setIsValidVisitResult($isValidVisitResult = null)
    {
        $this->IsValidVisitResult = $isValidVisitResult;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\IsValidVisitResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
