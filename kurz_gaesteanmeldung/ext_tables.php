<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'KURZ.KurzGaesteanmeldung',
            'Gaesteanmeldung',
            'Gästeanmeldung'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('kurz_gaesteanmeldung', 'Configuration/TypoScript', 'Guest registration form');

/*        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kurzgaesteanmeldung_domain_model_visitrequestdata', 'EXT:kurz_gaesteanmeldung/Resources/Private/Language/locallang_csh_tx_kurzgaesteanmeldung_domain_model_visitrequestdata.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kurzgaesteanmeldung_domain_model_visitrequestdata');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kurzgaesteanmeldung_domain_model_employeedata', 'EXT:kurz_gaesteanmeldung/Resources/Private/Language/locallang_csh_tx_kurzgaesteanmeldung_domain_model_employeedata.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kurzgaesteanmeldung_domain_model_employeedata');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kurzgaesteanmeldung_domain_model_guestdata', 'EXT:kurz_gaesteanmeldung/Resources/Private/Language/locallang_csh_tx_kurzgaesteanmeldung_domain_model_guestdata.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kurzgaesteanmeldung_domain_model_guestdata');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kurzgaesteanmeldung_domain_model_persondata', 'EXT:kurz_gaesteanmeldung/Resources/Private/Language/locallang_csh_tx_kurzgaesteanmeldung_domain_model_persondata.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kurzgaesteanmeldung_domain_model_persondata');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kurzgaesteanmeldung_domain_model_enterprisedata', 'EXT:kurz_gaesteanmeldung/Resources/Private/Language/locallang_csh_tx_kurzgaesteanmeldung_domain_model_enterprisedata.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kurzgaesteanmeldung_domain_model_enterprisedata');*/

    }
);
