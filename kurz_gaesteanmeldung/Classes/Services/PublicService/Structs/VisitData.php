<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for VisitData Structs
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:VisitData
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class VisitData
{
    /**
     * The AnsweredQuestions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnsweredQuestionData
     */
    public $AnsweredQuestions;
    /**
     * The Building
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Building;
    /**
     * The Employees
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfEmployeeData
     */
    public $Employees;
    /**
     * The ExtendedAnsweredQuestions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfExtendedAnsweredQuestionData
     */
    public $ExtendedAnsweredQuestions;
    /**
     * The Guests
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData
     */
    public $Guests;
    /**
     * The Id
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $Id;
    /**
     * The Notice
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Notice;
    /**
     * The Room
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Room;
    /**
     * The Schedule
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\ScheduleData
     */
    public $Schedule;
    /**
     * The ScheduledExitTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ScheduledExitTime;
    /**
     * The State
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $State;
    /**
     * The Type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $Type;
    /**
     * The Visitors
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitorData
     */
    public $Visitors;
    /**
     * Constructor method for VisitData
     * @uses VisitData::setAnsweredQuestions()
     * @uses VisitData::setBuilding()
     * @uses VisitData::setEmployees()
     * @uses VisitData::setExtendedAnsweredQuestions()
     * @uses VisitData::setGuests()
     * @uses VisitData::setId()
     * @uses VisitData::setNotice()
     * @uses VisitData::setRoom()
     * @uses VisitData::setSchedule()
     * @uses VisitData::setScheduledExitTime()
     * @uses VisitData::setState()
     * @uses VisitData::setType()
     * @uses VisitData::setVisitors()
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnsweredQuestionData $answeredQuestions
     * @param string $building
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfEmployeeData $employees
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfExtendedAnsweredQuestionData $extendedAnsweredQuestions
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData $guests
     * @param int $id
     * @param string $notice
     * @param string $room
     * @param \KURZ\VisitNet\PublicService\Structs\ScheduleData $schedule
     * @param string $scheduledExitTime
     * @param string $state
     * @param string $type
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitorData $visitors
     */
    public function __construct(\KURZ\VisitNet\PublicService\Arrays\ArrayOfAnsweredQuestionData $answeredQuestions = null, $building = null, \KURZ\VisitNet\PublicService\Arrays\ArrayOfEmployeeData $employees = null, \KURZ\VisitNet\PublicService\Arrays\ArrayOfExtendedAnsweredQuestionData $extendedAnsweredQuestions = null, \KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData $guests = null, $id = null, $notice = null, $room = null, \KURZ\VisitNet\PublicService\Structs\ScheduleData $schedule = null, $scheduledExitTime = null, $state = null, $type = null, \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitorData $visitors = null)
    {
        $this
            ->setAnsweredQuestions($answeredQuestions)
            ->setBuilding($building)
            ->setEmployees($employees)
            ->setExtendedAnsweredQuestions($extendedAnsweredQuestions)
            ->setGuests($guests)
            ->setId($id)
            ->setNotice($notice)
            ->setRoom($room)
            ->setSchedule($schedule)
            ->setScheduledExitTime($scheduledExitTime)
            ->setState($state)
            ->setType($type)
            ->setVisitors($visitors);
    }
    /**
     * Get AnsweredQuestions value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnsweredQuestionData|null
     */
    public function getAnsweredQuestions()
    {
        return isset($this->AnsweredQuestions) ? $this->AnsweredQuestions : null;
    }
    /**
     * Set AnsweredQuestions value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnsweredQuestionData $answeredQuestions
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public function setAnsweredQuestions(\KURZ\VisitNet\PublicService\Arrays\ArrayOfAnsweredQuestionData $answeredQuestions = null)
    {
        if (is_null($answeredQuestions) || (is_array($answeredQuestions) && empty($answeredQuestions))) {
            unset($this->AnsweredQuestions);
        } else {
            $this->AnsweredQuestions = $answeredQuestions;
        }
        return $this;
    }
    /**
     * Get Building value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBuilding()
    {
        return isset($this->Building) ? $this->Building : null;
    }
    /**
     * Set Building value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $building
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public function setBuilding($building = null)
    {
        if (is_null($building) || (is_array($building) && empty($building))) {
            unset($this->Building);
        } else {
            $this->Building = $building;
        }
        return $this;
    }
    /**
     * Get Employees value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfEmployeeData|null
     */
    public function getEmployees()
    {
        return isset($this->Employees) ? $this->Employees : null;
    }
    /**
     * Set Employees value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfEmployeeData $employees
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public function setEmployees(\KURZ\VisitNet\PublicService\Arrays\ArrayOfEmployeeData $employees = null)
    {
        if (is_null($employees) || (is_array($employees) && empty($employees))) {
            unset($this->Employees);
        } else {
            $this->Employees = $employees;
        }
        return $this;
    }
    /**
     * Get ExtendedAnsweredQuestions value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfExtendedAnsweredQuestionData|null
     */
    public function getExtendedAnsweredQuestions()
    {
        return isset($this->ExtendedAnsweredQuestions) ? $this->ExtendedAnsweredQuestions : null;
    }
    /**
     * Set ExtendedAnsweredQuestions value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfExtendedAnsweredQuestionData $extendedAnsweredQuestions
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public function setExtendedAnsweredQuestions(\KURZ\VisitNet\PublicService\Arrays\ArrayOfExtendedAnsweredQuestionData $extendedAnsweredQuestions = null)
    {
        if (is_null($extendedAnsweredQuestions) || (is_array($extendedAnsweredQuestions) && empty($extendedAnsweredQuestions))) {
            unset($this->ExtendedAnsweredQuestions);
        } else {
            $this->ExtendedAnsweredQuestions = $extendedAnsweredQuestions;
        }
        return $this;
    }
    /**
     * Get Guests value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData|null
     */
    public function getGuests()
    {
        return isset($this->Guests) ? $this->Guests : null;
    }
    /**
     * Set Guests value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData $guests
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public function setGuests(\KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData $guests = null)
    {
        if (is_null($guests) || (is_array($guests) && empty($guests))) {
            unset($this->Guests);
        } else {
            $this->Guests = $guests;
        }
        return $this;
    }
    /**
     * Get Id value
     * @return int|null
     */
    public function getId()
    {
        return $this->Id;
    }
    /**
     * Set Id value
     * @param int $id
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public function setId($id = null)
    {
        $this->Id = $id;
        return $this;
    }
    /**
     * Get Notice value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNotice()
    {
        return isset($this->Notice) ? $this->Notice : null;
    }
    /**
     * Set Notice value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $notice
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public function setNotice($notice = null)
    {
        if (is_null($notice) || (is_array($notice) && empty($notice))) {
            unset($this->Notice);
        } else {
            $this->Notice = $notice;
        }
        return $this;
    }
    /**
     * Get Room value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRoom()
    {
        return isset($this->Room) ? $this->Room : null;
    }
    /**
     * Set Room value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $room
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public function setRoom($room = null)
    {
        if (is_null($room) || (is_array($room) && empty($room))) {
            unset($this->Room);
        } else {
            $this->Room = $room;
        }
        return $this;
    }
    /**
     * Get Schedule value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\ScheduleData|null
     */
    public function getSchedule()
    {
        return isset($this->Schedule) ? $this->Schedule : null;
    }
    /**
     * Set Schedule value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\ScheduleData $schedule
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public function setSchedule(\KURZ\VisitNet\PublicService\Structs\ScheduleData $schedule = null)
    {
        if (is_null($schedule) || (is_array($schedule) && empty($schedule))) {
            unset($this->Schedule);
        } else {
            $this->Schedule = $schedule;
        }
        return $this;
    }
    /**
     * Get ScheduledExitTime value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getScheduledExitTime()
    {
        return isset($this->ScheduledExitTime) ? $this->ScheduledExitTime : null;
    }
    /**
     * Set ScheduledExitTime value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $scheduledExitTime
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public function setScheduledExitTime($scheduledExitTime = null)
    {
        if (is_null($scheduledExitTime) || (is_array($scheduledExitTime) && empty($scheduledExitTime))) {
            unset($this->ScheduledExitTime);
        } else {
            $this->ScheduledExitTime = $scheduledExitTime;
        }
        return $this;
    }
    /**
     * Get State value
     * @return string|null
     */
    public function getState()
    {
        return $this->State;
    }
    /**
     * Set State value
     * @param string $state
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public function setState($state = null)
    {
        $this->State = $state;
        return $this;
    }
    /**
     * Get Type value
     * @return string|null
     */
    public function getType()
    {
        return $this->Type;
    }
    /**
     * Set Type value
     * @param string $type
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public function setType($type = null)
    {
        $this->Type = $type;
        return $this;
    }
    /**
     * Get Visitors value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitorData|null
     */
    public function getVisitors()
    {
        return isset($this->Visitors) ? $this->Visitors : null;
    }
    /**
     * Set Visitors value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitorData $visitors
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public function setVisitors(\KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitorData $visitors = null)
    {
        if (is_null($visitors) || (is_array($visitors) && empty($visitors))) {
            unset($this->Visitors);
        } else {
            $this->Visitors = $visitors;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
