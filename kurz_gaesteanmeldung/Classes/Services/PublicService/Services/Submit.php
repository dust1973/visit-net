<?php

namespace KURZ\VisitNet\PublicService\Services;

use SoapClient;

/**
 * This class stands for Submit Services
 * @subpackage Services
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class Submit extends SoapClient
{
    /**
     * Method to call the operation originally named SubmitEntryWeight
     * @uses SoapClient::getSoapClient()
     * @uses SoapClient::setResult()
     * @uses SoapClient::getResult()
     * @uses SoapClient::saveLastError()
     * @param \KURZ\VisitNet\PublicService\Structs\SubmitEntryWeight $parameters
     * @return \KURZ\VisitNet\PublicService\Structs\SubmitEntryWeightResponse|bool
     */
    public function SubmitEntryWeight(\KURZ\VisitNet\PublicService\Structs\SubmitEntryWeight $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->SubmitEntryWeight($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named SubmitExitWeight
     * @uses SoapClient::getSoapClient()
     * @uses SoapClient::setResult()
     * @uses SoapClient::getResult()
     * @uses SoapClient::saveLastError()
     * @param \KURZ\VisitNet\PublicService\Structs\SubmitExitWeight $parameters
     * @return \KURZ\VisitNet\PublicService\Structs\SubmitExitWeightResponse|bool
     */
    public function SubmitExitWeight(\KURZ\VisitNet\PublicService\Structs\SubmitExitWeight $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->SubmitExitWeight($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see SoapClient::getResult()
     * @return \KURZ\VisitNet\PublicService\Structs\SubmitEntryWeightResponse|\KURZ\VisitNet\PublicService\Structs\SubmitExitWeightResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
