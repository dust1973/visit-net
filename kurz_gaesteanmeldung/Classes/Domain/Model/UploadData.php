<?php


namespace KURZ\KurzGaesteanmeldung\Domain\Model;


class UploadData
{
    /**
     * file
     *
     * @var \KURZ\KurzGaesteanmeldung\Domain\Model\FileReference
     * @cascade remove
     */
    protected $file = null;
}