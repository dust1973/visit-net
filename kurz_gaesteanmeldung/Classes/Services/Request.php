<?php

namespace KURZ\KurzGaesteanmeldung\Services;

use SoapClient;

/**
 * This class stands for Request Services
 * @subpackage Services
 * @date January 29, 2019, 1:29 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class Request extends VisitNetSoapClient
{
    /**
     * Method to call the operation originally named RequestVisit
     * @uses SoapClient::getSoapClient()
     * @uses SoapClient::setResult()
     * @uses SoapClient::getResult()
     * @uses SoapClient::saveLastError()
     * @param \KURZ\KurzGaesteanmeldung\Domain\Model\VisitRequestData $parameters
     * @return \KURZ\KurzGaesteanmeldung\Domain\Model\RequestVisitResponse|bool
     */
    public function RequestVisit(\KURZ\KurzGaesteanmeldung\Domain\Model\RequestVisit $parameters)
    {
        try {
            $this->setSoapClient();
            $res = $this->getSoapClient()->RequestVisit($parameters);
            $lastRequest = $this->getSoapClient()->__getLastRequest();
            $this->setResult($res);
            return $this->getResult();
        } catch (\SoapFault $soapFault) {

            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see SoapClient::getResult()
     * @return \KURZ\KurzGaesteanmeldung\Domain\Model\RequestVisitResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }

    private function saveLastError(string $__METHOD__, $soapFault)
    {
        var_dump($__METHOD__, $soapFault);
    }
}
