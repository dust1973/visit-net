<?php

namespace KURZ\VisitNet\PublicService;

/**
 * This class stands for VisitState
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:VisitState
 * @subpackage Enumerations
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class VisitState
{
    /**
     * Constant for value 'Scheduled'
     * @return string 'Scheduled'
     */
    const VALUE_SCHEDULED = 'Scheduled';
    /**
     * Constant for value 'Arrived'
     * @return string 'Arrived'
     */
    const VALUE_ARRIVED = 'Arrived';
    /**
     * Constant for value 'Begun'
     * @return string 'Begun'
     */
    const VALUE_BEGUN = 'Begun';
    /**
     * Constant for value 'Ended'
     * @return string 'Ended'
     */
    const VALUE_ENDED = 'Ended';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_SCHEDULED
     * @uses self::VALUE_ARRIVED
     * @uses self::VALUE_BEGUN
     * @uses self::VALUE_ENDED
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_SCHEDULED,
            self::VALUE_ARRIVED,
            self::VALUE_BEGUN,
            self::VALUE_ENDED,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
