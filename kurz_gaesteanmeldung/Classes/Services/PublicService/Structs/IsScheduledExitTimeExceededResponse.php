<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for IsScheduledExitTimeExceededResponse Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class IsScheduledExitTimeExceededResponse
{
    /**
     * The IsScheduledExitTimeExceededResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $IsScheduledExitTimeExceededResult;
    /**
     * Constructor method for IsScheduledExitTimeExceededResponse
     * @uses IsScheduledExitTimeExceededResponse::setIsScheduledExitTimeExceededResult()
     * @param bool $isScheduledExitTimeExceededResult
     */
    public function __construct($isScheduledExitTimeExceededResult = null)
    {
        $this
            ->setIsScheduledExitTimeExceededResult($isScheduledExitTimeExceededResult);
    }
    /**
     * Get IsScheduledExitTimeExceededResult value
     * @return bool|null
     */
    public function getIsScheduledExitTimeExceededResult()
    {
        return $this->IsScheduledExitTimeExceededResult;
    }
    /**
     * Set IsScheduledExitTimeExceededResult value
     * @param bool $isScheduledExitTimeExceededResult
     * @return \KURZ\VisitNet\PublicService\Structs\IsScheduledExitTimeExceededResponse
     */
    public function setIsScheduledExitTimeExceededResult($isScheduledExitTimeExceededResult = null)
    {
        $this->IsScheduledExitTimeExceededResult = $isScheduledExitTimeExceededResult;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\IsScheduledExitTimeExceededResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
