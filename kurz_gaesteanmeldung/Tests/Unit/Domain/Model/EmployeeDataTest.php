<?php
namespace KURZ\KurzGaesteanmeldung\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>
 */
class EmployeeDataTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \KURZ\KurzGaesteanmeldung\Domain\Model\EmployeeData
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \KURZ\KurzGaesteanmeldung\Domain\Model\EmployeeData();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getBuildingReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBuilding()
        );
    }

    /**
     * @test
     */
    public function setBuildingForStringSetsBuilding()
    {
        $this->subject->setBuilding('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'building',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCellPhoneReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getCellPhone()
        );
    }

    /**
     * @test
     */
    public function setCellPhoneForStringSetsCellPhone()
    {
        $this->subject->setCellPhone('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'cellPhone',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDepartmentReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDepartment()
        );
    }

    /**
     * @test
     */
    public function setDepartmentForStringSetsDepartment()
    {
        $this->subject->setDepartment('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'department',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDivisionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDivision()
        );
    }

    /**
     * @test
     */
    public function setDivisionForStringSetsDivision()
    {
        $this->subject->setDivision('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'division',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getOfficePhoneReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getOfficePhone()
        );
    }

    /**
     * @test
     */
    public function setOfficePhoneForStringSetsOfficePhone()
    {
        $this->subject->setOfficePhone('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'officePhone',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getOrganisationalUnitNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getOrganisationalUnitName()
        );
    }

    /**
     * @test
     */
    public function setOrganisationalUnitNameForStringSetsOrganisationalUnitName()
    {
        $this->subject->setOrganisationalUnitName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'organisationalUnitName',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRoomReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRoom()
        );
    }

    /**
     * @test
     */
    public function setRoomForStringSetsRoom()
    {
        $this->subject->setRoom('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'room',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPersonDataReturnsInitialValueForPersonData()
    {
        self::assertEquals(
            null,
            $this->subject->getPersonData()
        );
    }

    /**
     * @test
     */
    public function setPersonDataForPersonDataSetsPersonData()
    {
        $personDataFixture = new \KURZ\KurzGaesteanmeldung\Domain\Model\PersonData();
        $this->subject->setPersonData($personDataFixture);

        self::assertAttributeEquals(
            $personDataFixture,
            'personData',
            $this->subject
        );
    }
}
