<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'KURZ.KurzGaesteanmeldung',
            'Gaesteanmeldung',
            [
                'VisitRequestData' => 'new, create, showForm, sendMail'
            ],
            // non-cacheable actions
            [
                'VisitRequestData' => 'new, create, showForm, sendMail'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    gaesteanmeldung {
                        iconIdentifier = kurz_gaesteanmeldung-plugin-gaesteanmeldung
                        title = LLL:EXT:kurz_gaesteanmeldung/Resources/Private/Language/locallang_db.xlf:tx_kurz_gaesteanmeldung_gaesteanmeldung.name
                        description = LLL:EXT:kurz_gaesteanmeldung/Resources/Private/Language/locallang_db.xlf:tx_kurz_gaesteanmeldung_gaesteanmeldung.description
                        tt_content_defValues {
                            CType = list
                            list_type = kurzgaesteanmeldung_gaesteanmeldung
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'kurz_gaesteanmeldung-plugin-gaesteanmeldung',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:kurz_gaesteanmeldung/Resources/Public/Icons/user_plugin_gaesteanmeldung.svg']
			);
		
    }
);
