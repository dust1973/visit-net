<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for AutomobileData Structs
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:AutomobileData
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class AutomobileData
{
    /**
     * The Enterprise
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\EnterpriseData
     */
    public $Enterprise;
    /**
     * The LicensePlate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $LicensePlate;
    /**
     * The Type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $Type;
    /**
     * Constructor method for AutomobileData
     * @uses AutomobileData::setEnterprise()
     * @uses AutomobileData::setLicensePlate()
     * @uses AutomobileData::setType()
     * @param \KURZ\VisitNet\PublicService\Structs\EnterpriseData $enterprise
     * @param string $licensePlate
     * @param string $type
     */
    public function __construct(\KURZ\VisitNet\PublicService\Structs\EnterpriseData $enterprise = null, $licensePlate = null, $type = null)
    {
        $this
            ->setEnterprise($enterprise)
            ->setLicensePlate($licensePlate)
            ->setType($type);
    }
    /**
     * Get Enterprise value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\EnterpriseData|null
     */
    public function getEnterprise()
    {
        return isset($this->Enterprise) ? $this->Enterprise : null;
    }
    /**
     * Set Enterprise value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\EnterpriseData $enterprise
     * @return \KURZ\VisitNet\PublicService\Structs\AutomobileData
     */
    public function setEnterprise(\KURZ\VisitNet\PublicService\Structs\EnterpriseData $enterprise = null)
    {
        if (is_null($enterprise) || (is_array($enterprise) && empty($enterprise))) {
            unset($this->Enterprise);
        } else {
            $this->Enterprise = $enterprise;
        }
        return $this;
    }
    /**
     * Get LicensePlate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLicensePlate()
    {
        return isset($this->LicensePlate) ? $this->LicensePlate : null;
    }
    /**
     * Set LicensePlate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $licensePlate
     * @return \KURZ\VisitNet\PublicService\Structs\AutomobileData
     */
    public function setLicensePlate($licensePlate = null)
    {
        if (is_null($licensePlate) || (is_array($licensePlate) && empty($licensePlate))) {
            unset($this->LicensePlate);
        } else {
            $this->LicensePlate = $licensePlate;
        }
        return $this;
    }
    /**
     * Get Type value
     * @return string|null
     */
    public function getType()
    {
        return $this->Type;
    }
    /**
     * Set Type value
     * @param string $type
     * @return \KURZ\VisitNet\PublicService\Structs\AutomobileData
     */
    public function setType($type = null)
    {
        $this->Type = $type;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\AutomobileData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
