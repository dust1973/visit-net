<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for RequestVisit Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class RequestVisit
{
    /**
     * The visitRequestData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\VisitRequestData
     */
    public $visitRequestData;
    /**
     * Constructor method for RequestVisit
     * @uses RequestVisit::setVisitRequestData()
     * @param \KURZ\VisitNet\PublicService\Structs\VisitRequestData $visitRequestData
     */
    public function __construct(\KURZ\VisitNet\PublicService\Structs\VisitRequestData $visitRequestData = null)
    {
        $this
            ->setVisitRequestData($visitRequestData);
    }
    /**
     * Get visitRequestData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\VisitRequestData|null
     */
    public function getVisitRequestData()
    {
        return isset($this->visitRequestData) ? $this->visitRequestData : null;
    }
    /**
     * Set visitRequestData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\VisitRequestData $visitRequestData
     * @return \KURZ\VisitNet\PublicService\Structs\RequestVisit
     */
    public function setVisitRequestData(\KURZ\VisitNet\PublicService\Structs\VisitRequestData $visitRequestData = null)
    {
        if (is_null($visitRequestData) || (is_array($visitRequestData) && empty($visitRequestData))) {
            unset($this->visitRequestData);
        } else {
            $this->visitRequestData = $visitRequestData;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\RequestVisit
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
