
plugin.tx_kurzgaesteanmeldung_gaesteanmeldung {
    view {
        # cat=plugin.tx_kurzgaesteanmeldung_gaesteanmeldung/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:kurz_gaesteanmeldung/Resources/Private/Templates/
        # cat=plugin.tx_kurzgaesteanmeldung_gaesteanmeldung/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:kurz_gaesteanmeldung/Resources/Private/Partials/
        # cat=plugin.tx_kurzgaesteanmeldung_gaesteanmeldung/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:kurz_gaesteanmeldung/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_kurzgaesteanmeldung_gaesteanmeldung//a; type=string; label=Default storage PID
        storagePid =
    }
}
