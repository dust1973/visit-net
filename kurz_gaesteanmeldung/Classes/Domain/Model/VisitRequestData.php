<?php
namespace KURZ\KurzGaesteanmeldung\Domain\Model;

/***
 *
 * This file is part of the "Guest registration form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 *
 ***/

/**
 * VisitRequestData
 */
class VisitRequestData extends \KURZ\VisitNet\PublicService\Structs\VisitRequestData
{
    /**
     * The ScheduleTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @validate NotEmpty
     * @var string
     */
    public $ScheduleTime;

    /**
     * The ScheduledExitTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @validate NotEmpty
     * @var string
     */
    public $ScheduledExitTime;


    /**
     * The ShowOnWelcomeMonitor
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @validate NotEmpty
     * @var bool
     */
    public $ShowOnWelcomeMonitor;

    /**
     * The Employee
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\EmployeeData
     * @validate NotEmpty
     */
    public $Employee;

    /**
     * The Announcer
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\EmployeeData
     * @validate NotEmpty
     */
    public $Announcer;

    /**
     * The Visitors
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData
     */
    public $Visitors;

}
