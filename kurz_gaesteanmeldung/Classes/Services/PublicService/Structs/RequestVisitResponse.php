<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for RequestVisitResponse Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class RequestVisitResponse
{
    /**
     * The RequestVisitResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $RequestVisitResult;
    /**
     * Constructor method for RequestVisitResponse
     * @uses RequestVisitResponse::setRequestVisitResult()
     * @param bool $requestVisitResult
     */
    public function __construct($requestVisitResult = null)
    {
        $this
            ->setRequestVisitResult($requestVisitResult);
    }
    /**
     * Get RequestVisitResult value
     * @return bool|null
     */
    public function getRequestVisitResult()
    {
        return $this->RequestVisitResult;
    }
    /**
     * Set RequestVisitResult value
     * @param bool $requestVisitResult
     * @return \KURZ\VisitNet\PublicService\Structs\RequestVisitResponse
     */
    public function setRequestVisitResult($requestVisitResult = null)
    {
        $this->RequestVisitResult = $requestVisitResult;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\RequestVisitResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
