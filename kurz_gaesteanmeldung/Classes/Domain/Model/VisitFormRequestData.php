<?php


namespace KURZ\KurzGaesteanmeldung\Domain\Model;
/***
 *
 * This file is part of the "Guest registration form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 *
 ***/

/**
 * VisitFormRequestData
 */

class VisitFormRequestData
{

    /**
     * @var \KURZ\KurzGaesteanmeldung\Domain\Model\AdditionalData $additionalData
     */
    protected $additionalData;


    /**
     * @var bool
     */
    protected $receiveCopy;


    /**
     * @var \KURZ\KurzGaesteanmeldung\Domain\Model\VisitRequestData $visitRequestData
     */
    protected $visitRequestData;


    /**
     * file
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    protected $file;


    /**
     * Returns the file
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $file
     */
    public function getFile() {
        return $this->file;
    }


    /**
     * Sets the file
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $file
     * @return void
     */
    public function setFile($file) {
        $this->file = $file;
    }


    /**
     * @return \KURZ\KurzGaesteanmeldung\Domain\Model\AdditionalData $additionalData
     */
    public function getAdditionalData()
    {
        return $this->additionalData;
    }

    /**
     * @param \KURZ\KurzGaesteanmeldung\Domain\Model\AdditionalData $additionalData
     * @return void
     */
    public function setAdditionalData(\KURZ\KurzGaesteanmeldung\Domain\Model\AdditionalData $additionalData)
    {
        $this->additionalData = $additionalData;
    }

    /**
     * @return \KURZ\KurzGaesteanmeldung\Domain\Model\VisitRequestData $visitRequestData
     */
    public function getVisitRequestData()
    {
        return $this->visitRequestData;
    }

    /**
     * @param \KURZ\KurzGaesteanmeldung\Domain\Model\VisitRequestData $visitRequestData
     * @return void
     */
    public function setVisitRequestData(\KURZ\KurzGaesteanmeldung\Domain\Model\VisitRequestData $visitRequestData)
    {
        $this->visitRequestData = $visitRequestData;
    }

    /**
     * @return bool
     */
    public function isReceiveCopy()
    {
        return $this->receiveCopy;
    }

    /**
     * @param bool $receiveCopy
     */
    public function setReceiveCopy(bool $receiveCopy)
    {
        $this->receiveCopy = $receiveCopy;
    }






}