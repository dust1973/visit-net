<?php

namespace KURZ\VisitNet\PublicService\Arrays;

/**
 * This class stands for ArrayOfVisitData Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfVisitData
 * @subpackage Arrays
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class ArrayOfVisitData
{
    /**
     * The VisitData
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\VisitData[]
     */
    public $VisitData;
    /**
     * Constructor method for ArrayOfVisitData
     * @uses ArrayOfVisitData::setVisitData()
     * @param \KURZ\VisitNet\PublicService\Structs\VisitData[] $visitData
     */
    public function __construct(array $visitData = array())
    {
        $this
            ->setVisitData($visitData);
    }
    /**
     * Get VisitData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData[]|null
     */
    public function getVisitData()
    {
        return isset($this->VisitData) ? $this->VisitData : null;
    }
    /**
     * Set VisitData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\VisitData[] $visitData
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData
     */
    public function setVisitData(array $visitData = array())
    {
        if (is_null($visitData) || (is_array($visitData) && empty($visitData))) {
            unset($this->VisitData);
        } else {
            $this->VisitData = $visitData;
        }
        return $this;
    }
    /**
     * Add item to VisitData value
     * @throws \InvalidArgumentException
     * @param \KURZ\VisitNet\PublicService\Structs\VisitData $item
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData
     */
    public function addToVisitData(\KURZ\VisitNet\PublicService\Structs\VisitData $item)
    {
        $this->VisitData[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see ::current()
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see ::item()
     * @param int $index
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see ::first()
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see ::last()
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see ::offsetGet()
     * @param int $offset
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see ::getAttributeName()
     * @return string VisitData
     */
    public function getAttributeName()
    {
        return 'VisitData';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
