<?php
namespace KURZ\KurzGaesteanmeldung\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>
 */
class VisitRequestDataTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \KURZ\KurzGaesteanmeldung\Domain\Model\VisitRequestData
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \KURZ\KurzGaesteanmeldung\Domain\Model\VisitRequestData();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getAdditionalInformationForSecurityStaffReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAdditionalInformationForSecurityStaff()
        );
    }

    /**
     * @test
     */
    public function setAdditionalInformationForSecurityStaffForStringSetsAdditionalInformationForSecurityStaff()
    {
        $this->subject->setAdditionalInformationForSecurityStaff('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'additionalInformationForSecurityStaff',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAnonymousVisitorsReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getAnonymousVisitors()
        );
    }

    /**
     * @test
     */
    public function setAnonymousVisitorsForIntSetsAnonymousVisitors()
    {
        $this->subject->setAnonymousVisitors(12);

        self::assertAttributeEquals(
            12,
            'anonymousVisitors',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getBuildingReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBuilding()
        );
    }

    /**
     * @test
     */
    public function setBuildingForStringSetsBuilding()
    {
        $this->subject->setBuilding('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'building',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLocationNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLocationName()
        );
    }

    /**
     * @test
     */
    public function setLocationNameForStringSetsLocationName()
    {
        $this->subject->setLocationName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'locationName',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getScheduleTimeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getScheduleTime()
        );
    }

    /**
     * @test
     */
    public function setScheduleTimeForStringSetsScheduleTime()
    {
        $this->subject->setScheduleTime('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'scheduleTime',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAnnouncerReturnsInitialValueForEmployeeData()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getAnnouncer()
        );
    }

    /**
     * @test
     */
    public function setAnnouncerForObjectStorageContainingEmployeeDataSetsAnnouncer()
    {
        $announcer = new \KURZ\KurzGaesteanmeldung\Domain\Model\EmployeeData();
        $objectStorageHoldingExactlyOneAnnouncer = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneAnnouncer->attach($announcer);
        $this->subject->setAnnouncer($objectStorageHoldingExactlyOneAnnouncer);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneAnnouncer,
            'announcer',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addAnnouncerToObjectStorageHoldingAnnouncer()
    {
        $announcer = new \KURZ\KurzGaesteanmeldung\Domain\Model\EmployeeData();
        $announcerObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $announcerObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($announcer));
        $this->inject($this->subject, 'announcer', $announcerObjectStorageMock);

        $this->subject->addAnnouncer($announcer);
    }

    /**
     * @test
     */
    public function removeAnnouncerFromObjectStorageHoldingAnnouncer()
    {
        $announcer = new \KURZ\KurzGaesteanmeldung\Domain\Model\EmployeeData();
        $announcerObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $announcerObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($announcer));
        $this->inject($this->subject, 'announcer', $announcerObjectStorageMock);

        $this->subject->removeAnnouncer($announcer);
    }

    /**
     * @test
     */
    public function getEmployeeReturnsInitialValueForEmployeeData()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getEmployee()
        );
    }

    /**
     * @test
     */
    public function setEmployeeForObjectStorageContainingEmployeeDataSetsEmployee()
    {
        $employee = new \KURZ\KurzGaesteanmeldung\Domain\Model\EmployeeData();
        $objectStorageHoldingExactlyOneEmployee = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneEmployee->attach($employee);
        $this->subject->setEmployee($objectStorageHoldingExactlyOneEmployee);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneEmployee,
            'employee',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addEmployeeToObjectStorageHoldingEmployee()
    {
        $employee = new \KURZ\KurzGaesteanmeldung\Domain\Model\EmployeeData();
        $employeeObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $employeeObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($employee));
        $this->inject($this->subject, 'employee', $employeeObjectStorageMock);

        $this->subject->addEmployee($employee);
    }

    /**
     * @test
     */
    public function removeEmployeeFromObjectStorageHoldingEmployee()
    {
        $employee = new \KURZ\KurzGaesteanmeldung\Domain\Model\EmployeeData();
        $employeeObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $employeeObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($employee));
        $this->inject($this->subject, 'employee', $employeeObjectStorageMock);

        $this->subject->removeEmployee($employee);
    }

    /**
     * @test
     */
    public function getEmployeeDeputyReturnsInitialValueForEmployeeData()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getEmployeeDeputy()
        );
    }

    /**
     * @test
     */
    public function setEmployeeDeputyForObjectStorageContainingEmployeeDataSetsEmployeeDeputy()
    {
        $employeeDeputy = new \KURZ\KurzGaesteanmeldung\Domain\Model\EmployeeData();
        $objectStorageHoldingExactlyOneEmployeeDeputy = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneEmployeeDeputy->attach($employeeDeputy);
        $this->subject->setEmployeeDeputy($objectStorageHoldingExactlyOneEmployeeDeputy);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneEmployeeDeputy,
            'employeeDeputy',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addEmployeeDeputyToObjectStorageHoldingEmployeeDeputy()
    {
        $employeeDeputy = new \KURZ\KurzGaesteanmeldung\Domain\Model\EmployeeData();
        $employeeDeputyObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $employeeDeputyObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($employeeDeputy));
        $this->inject($this->subject, 'employeeDeputy', $employeeDeputyObjectStorageMock);

        $this->subject->addEmployeeDeputy($employeeDeputy);
    }

    /**
     * @test
     */
    public function removeEmployeeDeputyFromObjectStorageHoldingEmployeeDeputy()
    {
        $employeeDeputy = new \KURZ\KurzGaesteanmeldung\Domain\Model\EmployeeData();
        $employeeDeputyObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $employeeDeputyObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($employeeDeputy));
        $this->inject($this->subject, 'employeeDeputy', $employeeDeputyObjectStorageMock);

        $this->subject->removeEmployeeDeputy($employeeDeputy);
    }

    /**
     * @test
     */
    public function getVisitorsReturnsInitialValueForGuestData()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getVisitors()
        );
    }

    /**
     * @test
     */
    public function setVisitorsForObjectStorageContainingGuestDataSetsVisitors()
    {
        $visitor = new \KURZ\KurzGaesteanmeldung\Domain\Model\GuestData();
        $objectStorageHoldingExactlyOneVisitors = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneVisitors->attach($visitor);
        $this->subject->setVisitors($objectStorageHoldingExactlyOneVisitors);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneVisitors,
            'visitors',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addVisitorToObjectStorageHoldingVisitors()
    {
        $visitor = new \KURZ\KurzGaesteanmeldung\Domain\Model\GuestData();
        $visitorsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $visitorsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($visitor));
        $this->inject($this->subject, 'visitors', $visitorsObjectStorageMock);

        $this->subject->addVisitor($visitor);
    }

    /**
     * @test
     */
    public function removeVisitorFromObjectStorageHoldingVisitors()
    {
        $visitor = new \KURZ\KurzGaesteanmeldung\Domain\Model\GuestData();
        $visitorsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $visitorsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($visitor));
        $this->inject($this->subject, 'visitors', $visitorsObjectStorageMock);

        $this->subject->removeVisitor($visitor);
    }
}
