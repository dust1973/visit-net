<?php
namespace KURZ\KurzGaesteanmeldung\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>
 */
class VisitRequestDataControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \KURZ\KurzGaesteanmeldung\Controller\VisitRequestDataController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\KURZ\KurzGaesteanmeldung\Controller\VisitRequestDataController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenVisitRequestDataToVisitRequestDataRepository()
    {
        $visitRequestData = new \KURZ\KurzGaesteanmeldung\Domain\Model\VisitRequestData();

        $visitRequestDataRepository = $this->getMockBuilder(\KURZ\KurzGaesteanmeldung\Domain\Repository\VisitRequestDataRepository::class)
            ->setMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $visitRequestDataRepository->expects(self::once())->method('add')->with($visitRequestData);
        $this->inject($this->subject, 'visitRequestDataRepository', $visitRequestDataRepository);

        $this->subject->createAction($visitRequestData);
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenVisitRequestDataToView()
    {
        $visitRequestData = new \KURZ\KurzGaesteanmeldung\Domain\Model\VisitRequestData();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('visitRequestData', $visitRequestData);

        $this->subject->showAction($visitRequestData);
    }
}
