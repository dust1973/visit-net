<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for VisitorData Structs
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:VisitorData
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class VisitorData
{
    /**
     * The AnsweredQuestions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnsweredQuestionData
     */
    public $AnsweredQuestions;
    /**
     * The Automobile
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\AutomobileData
     */
    public $Automobile;
    /**
     * The ExtendedAnsweredQuestions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfExtendedAnsweredQuestionData
     */
    public $ExtendedAnsweredQuestions;
    /**
     * The Guest
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\GuestData
     */
    public $Guest;
    /**
     * Constructor method for VisitorData
     * @uses VisitorData::setAnsweredQuestions()
     * @uses VisitorData::setAutomobile()
     * @uses VisitorData::setExtendedAnsweredQuestions()
     * @uses VisitorData::setGuest()
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnsweredQuestionData $answeredQuestions
     * @param \KURZ\VisitNet\PublicService\Structs\AutomobileData $automobile
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfExtendedAnsweredQuestionData $extendedAnsweredQuestions
     * @param \KURZ\VisitNet\PublicService\Structs\GuestData $guest
     */
    public function __construct(\KURZ\VisitNet\PublicService\Arrays\ArrayOfAnsweredQuestionData $answeredQuestions = null, \KURZ\VisitNet\PublicService\Structs\AutomobileData $automobile = null, \KURZ\VisitNet\PublicService\Arrays\ArrayOfExtendedAnsweredQuestionData $extendedAnsweredQuestions = null, \KURZ\VisitNet\PublicService\Structs\GuestData $guest = null)
    {
        $this
            ->setAnsweredQuestions($answeredQuestions)
            ->setAutomobile($automobile)
            ->setExtendedAnsweredQuestions($extendedAnsweredQuestions)
            ->setGuest($guest);
    }
    /**
     * Get AnsweredQuestions value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnsweredQuestionData|null
     */
    public function getAnsweredQuestions()
    {
        return isset($this->AnsweredQuestions) ? $this->AnsweredQuestions : null;
    }
    /**
     * Set AnsweredQuestions value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnsweredQuestionData $answeredQuestions
     * @return \KURZ\VisitNet\PublicService\Structs\VisitorData
     */
    public function setAnsweredQuestions(\KURZ\VisitNet\PublicService\Arrays\ArrayOfAnsweredQuestionData $answeredQuestions = null)
    {
        if (is_null($answeredQuestions) || (is_array($answeredQuestions) && empty($answeredQuestions))) {
            unset($this->AnsweredQuestions);
        } else {
            $this->AnsweredQuestions = $answeredQuestions;
        }
        return $this;
    }
    /**
     * Get Automobile value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\AutomobileData|null
     */
    public function getAutomobile()
    {
        return isset($this->Automobile) ? $this->Automobile : null;
    }
    /**
     * Set Automobile value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\AutomobileData $automobile
     * @return \KURZ\VisitNet\PublicService\Structs\VisitorData
     */
    public function setAutomobile(\KURZ\VisitNet\PublicService\Structs\AutomobileData $automobile = null)
    {
        if (is_null($automobile) || (is_array($automobile) && empty($automobile))) {
            unset($this->Automobile);
        } else {
            $this->Automobile = $automobile;
        }
        return $this;
    }
    /**
     * Get ExtendedAnsweredQuestions value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfExtendedAnsweredQuestionData|null
     */
    public function getExtendedAnsweredQuestions()
    {
        return isset($this->ExtendedAnsweredQuestions) ? $this->ExtendedAnsweredQuestions : null;
    }
    /**
     * Set ExtendedAnsweredQuestions value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfExtendedAnsweredQuestionData $extendedAnsweredQuestions
     * @return \KURZ\VisitNet\PublicService\Structs\VisitorData
     */
    public function setExtendedAnsweredQuestions(\KURZ\VisitNet\PublicService\Arrays\ArrayOfExtendedAnsweredQuestionData $extendedAnsweredQuestions = null)
    {
        if (is_null($extendedAnsweredQuestions) || (is_array($extendedAnsweredQuestions) && empty($extendedAnsweredQuestions))) {
            unset($this->ExtendedAnsweredQuestions);
        } else {
            $this->ExtendedAnsweredQuestions = $extendedAnsweredQuestions;
        }
        return $this;
    }
    /**
     * Get Guest value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\GuestData|null
     */
    public function getGuest()
    {
        return isset($this->Guest) ? $this->Guest : null;
    }
    /**
     * Set Guest value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\GuestData $guest
     * @return \KURZ\VisitNet\PublicService\Structs\VisitorData
     */
    public function setGuest(\KURZ\VisitNet\PublicService\Structs\GuestData $guest = null)
    {
        if (is_null($guest) || (is_array($guest) && empty($guest))) {
            unset($this->Guest);
        } else {
            $this->Guest = $guest;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\VisitorData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
