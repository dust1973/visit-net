<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for AnswerData Structs
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:AnswerData
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class AnswerData
{
    /**
     * The Id
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - ref: ser:Id
     * @var int
     */
    public $Id;
    /**
     * The Translations
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfTranslationData
     */
    public $Translations;
    /**
     * The Ref
     * Meta information extracted from the WSDL
     * - ref: ser:Ref
     * @var string
     */
    public $Ref;
    /**
     * Constructor method for AnswerData
     * @uses AnswerData::setId()
     * @uses AnswerData::setTranslations()
     * @uses AnswerData::setRef()
     * @param int $id
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfTranslationData $translations
     * @param string $ref
     */
    public function __construct($id = null, \KURZ\VisitNet\PublicService\Arrays\ArrayOfTranslationData $translations = null, $ref = null)
    {
        $this
            ->setId($id)
            ->setTranslations($translations)
            ->setRef($ref);
    }
    /**
     * Get Id value
     * @return int|null
     */
    public function getId()
    {
        return $this->Id;
    }
    /**
     * Set Id value
     * @param int $id
     * @return \KURZ\VisitNet\PublicService\Structs\AnswerData
     */
    public function setId($id = null)
    {
        $this->Id = $id;
        return $this;
    }
    /**
     * Get Translations value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfTranslationData|null
     */
    public function getTranslations()
    {
        return isset($this->Translations) ? $this->Translations : null;
    }
    /**
     * Set Translations value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfTranslationData $translations
     * @return \KURZ\VisitNet\PublicService\Structs\AnswerData
     */
    public function setTranslations(\KURZ\VisitNet\PublicService\Arrays\ArrayOfTranslationData $translations = null)
    {
        if (is_null($translations) || (is_array($translations) && empty($translations))) {
            unset($this->Translations);
        } else {
            $this->Translations = $translations;
        }
        return $this;
    }
    /**
     * Get Ref value
     * @return string|null
     */
    public function getRef()
    {
        return $this->Ref;
    }
    /**
     * Set Ref value
     * @param string $ref
     * @return \KURZ\VisitNet\PublicService\Structs\AnswerData
     */
    public function setRef($ref = null)
    {
        $this->Ref = $ref;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\AnswerData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
