<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for GetGuestDataByCodeResponse Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class GetGuestDataByCodeResponse
{
    /**
     * The GetGuestDataByCodeResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\GuestData
     */
    public $GetGuestDataByCodeResult;
    /**
     * Constructor method for GetGuestDataByCodeResponse
     * @uses GetGuestDataByCodeResponse::setGetGuestDataByCodeResult()
     * @param \KURZ\VisitNet\PublicService\Structs\GuestData $getGuestDataByCodeResult
     */
    public function __construct(\KURZ\VisitNet\PublicService\Structs\GuestData $getGuestDataByCodeResult = null)
    {
        $this
            ->setGetGuestDataByCodeResult($getGuestDataByCodeResult);
    }
    /**
     * Get GetGuestDataByCodeResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\GuestData|null
     */
    public function getGetGuestDataByCodeResult()
    {
        return isset($this->GetGuestDataByCodeResult) ? $this->GetGuestDataByCodeResult : null;
    }
    /**
     * Set GetGuestDataByCodeResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\GuestData $getGuestDataByCodeResult
     * @return \KURZ\VisitNet\PublicService\Structs\GetGuestDataByCodeResponse
     */
    public function setGetGuestDataByCodeResult(\KURZ\VisitNet\PublicService\Structs\GuestData $getGuestDataByCodeResult = null)
    {
        if (is_null($getGuestDataByCodeResult) || (is_array($getGuestDataByCodeResult) && empty($getGuestDataByCodeResult))) {
            unset($this->GetGuestDataByCodeResult);
        } else {
            $this->GetGuestDataByCodeResult = $getGuestDataByCodeResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\GetGuestDataByCodeResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
