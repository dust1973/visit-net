<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for AnsweredQuestionSingleSelectData Structs
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:AnsweredQuestionSingleSelectData
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class AnsweredQuestionSingleSelectData extends ExtendedAnsweredQuestionData
{
    /**
     * The AnswerData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\AnswerData
     */
    public $AnswerData;
    /**
     * Constructor method for AnsweredQuestionSingleSelectData
     * @uses AnsweredQuestionSingleSelectData::setAnswerData()
     * @param \KURZ\VisitNet\PublicService\Structs\AnswerData $answerData
     */
    public function __construct(\KURZ\VisitNet\PublicService\Structs\AnswerData $answerData = null)
    {
        $this
            ->setAnswerData($answerData);
    }
    /**
     * Get AnswerData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\AnswerData|null
     */
    public function getAnswerData()
    {
        return isset($this->AnswerData) ? $this->AnswerData : null;
    }
    /**
     * Set AnswerData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\AnswerData $answerData
     * @return \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionSingleSelectData
     */
    public function setAnswerData(\KURZ\VisitNet\PublicService\Structs\AnswerData $answerData = null)
    {
        if (is_null($answerData) || (is_array($answerData) && empty($answerData))) {
            unset($this->AnswerData);
        } else {
            $this->AnswerData = $answerData;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionSingleSelectData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
