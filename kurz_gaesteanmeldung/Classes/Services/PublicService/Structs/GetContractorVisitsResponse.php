<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for GetContractorVisitsResponse Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class GetContractorVisitsResponse
{
    /**
     * The GetContractorVisitsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData
     */
    public $GetContractorVisitsResult;
    /**
     * Constructor method for GetContractorVisitsResponse
     * @uses GetContractorVisitsResponse::setGetContractorVisitsResult()
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getContractorVisitsResult
     */
    public function __construct(\KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getContractorVisitsResult = null)
    {
        $this
            ->setGetContractorVisitsResult($getContractorVisitsResult);
    }
    /**
     * Get GetContractorVisitsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData|null
     */
    public function getGetContractorVisitsResult()
    {
        return isset($this->GetContractorVisitsResult) ? $this->GetContractorVisitsResult : null;
    }
    /**
     * Set GetContractorVisitsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getContractorVisitsResult
     * @return \KURZ\VisitNet\PublicService\Structs\GetContractorVisitsResponse
     */
    public function setGetContractorVisitsResult(\KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getContractorVisitsResult = null)
    {
        if (is_null($getContractorVisitsResult) || (is_array($getContractorVisitsResult) && empty($getContractorVisitsResult))) {
            unset($this->GetContractorVisitsResult);
        } else {
            $this->GetContractorVisitsResult = $getContractorVisitsResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\GetContractorVisitsResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
