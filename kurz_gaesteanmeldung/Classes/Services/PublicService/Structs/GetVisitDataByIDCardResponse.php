<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for GetVisitDataByIDCardResponse Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class GetVisitDataByIDCardResponse
{
    /**
     * The GetVisitDataByIDCardResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public $GetVisitDataByIDCardResult;
    /**
     * Constructor method for GetVisitDataByIDCardResponse
     * @uses GetVisitDataByIDCardResponse::setGetVisitDataByIDCardResult()
     * @param \KURZ\VisitNet\PublicService\Structs\VisitData $getVisitDataByIDCardResult
     */
    public function __construct(\KURZ\VisitNet\PublicService\Structs\VisitData $getVisitDataByIDCardResult = null)
    {
        $this
            ->setGetVisitDataByIDCardResult($getVisitDataByIDCardResult);
    }
    /**
     * Get GetVisitDataByIDCardResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData|null
     */
    public function getGetVisitDataByIDCardResult()
    {
        return isset($this->GetVisitDataByIDCardResult) ? $this->GetVisitDataByIDCardResult : null;
    }
    /**
     * Set GetVisitDataByIDCardResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\VisitData $getVisitDataByIDCardResult
     * @return \KURZ\VisitNet\PublicService\Structs\GetVisitDataByIDCardResponse
     */
    public function setGetVisitDataByIDCardResult(\KURZ\VisitNet\PublicService\Structs\VisitData $getVisitDataByIDCardResult = null)
    {
        if (is_null($getVisitDataByIDCardResult) || (is_array($getVisitDataByIDCardResult) && empty($getVisitDataByIDCardResult))) {
            unset($this->GetVisitDataByIDCardResult);
        } else {
            $this->GetVisitDataByIDCardResult = $getVisitDataByIDCardResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\GetVisitDataByIDCardResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
