<?php

namespace KURZ\KurzGaesteanmeldung\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\MvcPropertyMappingConfiguration;
use TYPO3\CMS\Fluid\View\StandaloneView;
use KURZ\KurzGaesteanmeldung\Domain\Model\RequestVisit;
use KURZ\KurzGaesteanmeldung\Services\Request;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;

/***
 *
 * This file is part of the "Guest registration form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 *
 ***/

/**
 * VisitRequestDataController
 */
class VisitRequestDataController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * relatedVisit
     *
     * @var string
     */
    protected $relatedVisit = null;

    /**
     * layoutRootPaths
     * @var array
     */
    protected $layoutRootPaths = [];

    /**
     * partialRootPaths
     * @var array
     */
    protected $partialRootPaths = [];

    /**
     * templateRootPaths
     * @var array
     */
    protected $templateRootPaths = [];


    /**
     * @var MvcPropertyMappingConfiguration
     */
    protected $propertyMappingConfiguration;


    /**
     * A standalone template view
     *
     * @Inject
     * @var StandaloneView
     */
    protected $standaloneView;

    /**
     * initialize new action
     *
     * @return void
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws NoSuchArgumentException
     */
    public function initializeCreateAction()
    {

        if ($this->arguments->hasArgument('newVisitFormRequestData')) {
            try {
                $newVisitFormRequestData = $this->request->getArgument('newVisitFormRequestData');
            } catch (NoSuchArgumentException $e) {
            }

            $this->propertyMappingConfiguration = $this->arguments
                ->getArgument('newVisitFormRequestData')
                ->getPropertyMappingConfiguration();

            $this->propertyMappingConfiguration->skipProperties('visitRequestData.AdditionalInformationForSecurityStaff');
              //  ->allowProperties('receiveCopy');
              // ->skipProperties('receiveCopy');

            ///->skipProperties('visitRequestData.ShowOnWelcomeMonitor');

            $this->propertyMappingConfiguration->setTargetTypeForSubProperty('file', 'array');
            $this->propertyMappingConfiguration->allowCreationForSubProperty('file');

            // Type converter for property ScheduleTime string -> to DateTime
            $this->propertyMappingConfiguration
                ->forProperty('visitRequestData.ScheduleTime')
                ->setTypeConverterOption('TYPO3\\CMS\\Extbase\\Property\\TypeConverter\\DateTimeConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'd-m-Y H:i');

            // Type converter for property ScheduledExitTime string -> to DateTime
            $this->propertyMappingConfiguration
                ->forProperty('visitRequestData.ScheduledExitTime')
                ->setTypeConverterOption('TYPO3\\CMS\\Extbase\\Property\\TypeConverter\\DateTimeConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'd-m-Y H:i');




            // Type converter for all sub property visitRequestData.Visitors.GuestData[]
            // visitRequestData.Visitors.GuestData[] array -> to object KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData
            // visitRequestData.Visitors.GuestData.i -> to object KURZ\VisitNet\PublicService\Structs\GuestData
            // visitRequestData.Visitors.GuestData.i.Company -> object KURZ\VisitNet\PublicService\Structs\EnterpriseData
            $i = 0;
            $guestData = array();
            $arrayOfGuestData = $this->objectManager->get('KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData');
            foreach ($newVisitFormRequestData['visitRequestData']['Visitors']['GuestData'] as $guest) {
                $Company = $this->objectManager->get(\TYPO3\CMS\Extbase\Property\PropertyMapper::class)
                    ->convert(
                        $newVisitFormRequestData['visitRequestData']['Visitors']['GuestData'][$i]['Company'],
                        'KURZ\VisitNet\PublicService\Structs\EnterpriseData'
                    );
                $guest['Company'] = $Company;
                $guestData[$i] = $this->objectManager->get(\TYPO3\CMS\Extbase\Property\PropertyMapper::class)
                    ->convert(
                        $guest,
                        'KURZ\VisitNet\PublicService\Structs\GuestData'
                    );
                $arrayOfGuestData->addToGuestData($guestData[$i]);
                $i++;
            }
            $newVisitFormRequestData['visitRequestData']['Visitors'] = $arrayOfGuestData;


            // Type converter for all sub property visitRequestData.Visitors
            // visitRequestData.Visitors array -> to object KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData

            $Visitors = $this->objectManager->get(\TYPO3\CMS\Extbase\Property\PropertyMapper::class)
                ->convert(
                    $newVisitFormRequestData['visitRequestData']['Visitors'],
                    'KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData'
                );
            $Visitors->setGuestData($guestData);
            $newVisitFormRequestData['visitRequestData']['Visitors'] = $Visitors;
            $newVisitFormRequestDataArguments['newVisitFormRequestData'] = $newVisitFormRequestData;
            $this->objectManager->get(\TYPO3\CMS\Extbase\Property\PropertyMapper::class)
                ->convert(
                    $newVisitFormRequestData['visitRequestData']['Visitors'],
                    'KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData'
                );

            $this->request->setArguments($newVisitFormRequestDataArguments);
        }
    }

    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {

    }


    /**
     * @param \KURZ\KurzGaesteanmeldung\Domain\Model\VisitFormRequestData $newVisitFormRequestData
     * @throws NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function createAction(\KURZ\KurzGaesteanmeldung\Domain\Model\VisitFormRequestData $newVisitFormRequestData)
    {
        $getVisitRequestData = $newVisitFormRequestData->getVisitRequestData();
        $additionalData = $newVisitFormRequestData->getAdditionalData();

        if ($getVisitRequestData->getShowOnWelcomeMonitor() == 'showOnWelcomeMonitor') {
            $getVisitRequestData->setShowOnWelcomeMonitor(true);
        } else {
            $getVisitRequestData->setShowOnWelcomeMonitor(false);
        }

        $isWifi = null != $additionalData->isWifi() ? 'Ja' : 'Nein';
        $isParking = null != $additionalData->isParking() ? 'Ja' : 'Nein';
        $miscellaneous = null != $additionalData->getMiscellaneous() ? ', Sonstiges: ' . $additionalData->getMiscellaneous() : '';
        $additionalInformationForSecurityStaff =
            'Bildschirmbegrüssung: ' . $getVisitRequestData->getShowOnWelcomeMonitor()
            . ', Parkplatz: ' . $isParking
            . ', WLAN: ' . $isWifi
            . $miscellaneous;


        $path = $_FILES['tx_kurzgaesteanmeldung_gaesteanmeldung']['tmp_name']['newVisitFormRequestData']['file'];
        $type = $_FILES['tx_kurzgaesteanmeldung_gaesteanmeldung']['type']['newVisitFormRequestData']['file'];
        $filename = $_FILES['tx_kurzgaesteanmeldung_gaesteanmeldung']['name']['newVisitFormRequestData']['file'];


        /// move_uploaded_file($_FILES['tx_kurzgaesteanmeldung_gaesteanmeldung']['tmp_name'], '/srv/www/cms/uploads/'.$_FILES['tx_kurzgaesteanmeldung_gaesteanmeldung']['name']['newVisitFormRequestData']['file']);

        //$data = file_get_contents($path);
        //$base64 = base64_encode($data);

        /*      $binaryFile = new BinaryFile();
                $binaryFile->setFileContent($base64);
                $binaryFile->setFileName($_FILES['name']);
                $binaryFile->setFileType($type);
                $binaryFile->setGuestId(7183);
                $getVisitRequestData->setAttachment($binaryFile);
        */

        $getVisitRequestData->setAdditionalInformationForSecurityStaff($additionalInformationForSecurityStaff);
        ///$newVisitFormRequestData->setAdditionalInformationForSecurityStaff($this->relatedVisit );
        ///
        $context = stream_context_create([
            'ssl' => [
                // set some SSL/TLS specific options
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ]);

        $soap_option = array(
            //'soap_version' => SOAP_1_1,
            'exceptions' => true,
            'trace' => 1,
            //'cache_wsdl' => WSDL_CACHE_NONE,
            'stream_context' => $context,
            'classmap' => \KURZ\VisitNet\PublicService\ClassMap::get()
        );
        $requestVisit = new RequestVisit();
        $requestVisit->setVisitRequestData($getVisitRequestData);
        ///http://defumss0820.kurz-group.com/VISIT.net_service/public/v1/publicservice.svc?wsdl
        $request = new Request('http://defumss0836.kurz-group.com/VISIT.net_demo_service/public/v1/PublicService.svc?wsdl', $soap_option);
        $res = $request->RequestVisit($requestVisit);
        if ($res) {
            $this->sendMail($path, $filename, $type, $requestVisit->getVisitRequestData(), $newVisitFormRequestData->isReceiveCopy());
            $this->addFlashMessage('Besuchsanfrage wurde erfolgreich versendet', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
        }
        /// $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check.', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        ///$this->visitRequestDataRepository->add($newVisitRequestData);
        $this->redirect('new');
    }

    /**
     *  sendMail
     *
     * @return void
     */
    public function sendMail($path, $filename, $type, $requestVisitData, $receiveCopy)
    {

        $visitors = $requestVisitData->getVisitors()->getGuestData();
        $this->standaloneView = GeneralUtility::makeInstance(StandaloneView::class);
        // set your template path

        $this->standaloneView->setFormat('html');
        $this->standaloneView->setTemplatePathAndFilename(GeneralUtility::getFileAbsFileName('EXT:kurz_gaesteanmeldung/Resources/Private/Templates/Email/HtmlEmail.html'));
        $this->standaloneView->assignMultiple([
            'VisitDate' => date('d-m-Y H:i:s', $requestVisitData->getScheduleTime()),
            'GuestData' => $visitors,
            'Announcer' => $requestVisitData->getAnnouncer(),
            'Employee' => $requestVisitData->getEmployee(),
            'AddInfo' => $requestVisitData->getAdditionalInformationForSecurityStaff(),
            'EmployeeDeputy' => $requestVisitData->getEmployeeDeputy()
        ]);


        $emailBody = $this->standaloneView->render();


        // Create the message
        $mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
        $mail->setSubject('VISIT.net - Benachrichtigung: Besuchsanfrage (' . $visitors[0]->getLastName() . ') für den ' . date('d-m-Y H:i:s', $requestVisitData->getScheduleTime()));
        $mail->setFrom(array('Besuchermanagement@kurz.de' => 'Besuchermanagement'));

        $mailTo = array('alexander.fuchs@kurz.de');
        if ($receiveCopy) {
            $mailTo = array('alexander.fuchs@kurz.de', $requestVisitData->getAnnouncer()->getEMailAddress());
        }
        $mail->setTo($mailTo);
        /// $mail->addPart('<q>Here is the message itself</q>', 'text/html');

        /// HTML Email
        $mail->setBody($emailBody, 'text/html');
        if ($path) {
            // Create the attachment, the content-type parameter is optional
            $attachment = \Swift_Attachment::fromPath($path, $type);
            // Set the filename (optional)
            $attachment->setFilename($filename);

            $mail->attach($attachment);
        }
        $mail->send();
    }

    /**
     * action showForm
     *
     * @return void
     */
    public function showFormAction()
    {

    }

    /**
     * Fehlermeldung unterdrücken
     *
     * @return bool|string
     */
    protected function getErrorFlashMessage()
    {
        return false;
    }


}
