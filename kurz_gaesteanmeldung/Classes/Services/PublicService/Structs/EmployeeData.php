<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for EmployeeData Structs
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:EmployeeData
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class EmployeeData extends PersonData
{
    /**
     * The Building
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Building;
    /**
     * The CellPhone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CellPhone;
    /**
     * The Department
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Department;
    /**
     * The Division
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Division;
    /**
     * The OfficePhone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $OfficePhone;
    /**
     * The OrganisationalUnitName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $OrganisationalUnitName;
    /**
     * The Room
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Room;
    /**
     * Constructor method for EmployeeData
     * @uses EmployeeData::setBuilding()
     * @uses EmployeeData::setCellPhone()
     * @uses EmployeeData::setDepartment()
     * @uses EmployeeData::setDivision()
     * @uses EmployeeData::setOfficePhone()
     * @uses EmployeeData::setOrganisationalUnitName()
     * @uses EmployeeData::setRoom()
     * @param string $building
     * @param string $cellPhone
     * @param string $department
     * @param string $division
     * @param string $officePhone
     * @param string $organisationalUnitName
     * @param string $room
     */
    public function __construct($building = null, $cellPhone = null, $department = null, $division = null, $officePhone = null, $organisationalUnitName = null, $room = null)
    {
        $this
            ->setBuilding($building)
            ->setCellPhone($cellPhone)
            ->setDepartment($department)
            ->setDivision($division)
            ->setOfficePhone($officePhone)
            ->setOrganisationalUnitName($organisationalUnitName)
            ->setRoom($room);
    }
    /**
     * Get Building value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBuilding()
    {
        return isset($this->Building) ? $this->Building : null;
    }
    /**
     * Set Building value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $building
     * @return \KURZ\VisitNet\PublicService\Structs\EmployeeData
     */
    public function setBuilding($building = null)
    {
        if (is_null($building) || (is_array($building) && empty($building))) {
            unset($this->Building);
        } else {
            $this->Building = $building;
        }
        return $this;
    }
    /**
     * Get CellPhone value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCellPhone()
    {
        return isset($this->CellPhone) ? $this->CellPhone : null;
    }
    /**
     * Set CellPhone value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $cellPhone
     * @return \KURZ\VisitNet\PublicService\Structs\EmployeeData
     */
    public function setCellPhone($cellPhone = null)
    {
        if (is_null($cellPhone) || (is_array($cellPhone) && empty($cellPhone))) {
            unset($this->CellPhone);
        } else {
            $this->CellPhone = $cellPhone;
        }
        return $this;
    }
    /**
     * Get Department value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDepartment()
    {
        return isset($this->Department) ? $this->Department : null;
    }
    /**
     * Set Department value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $department
     * @return \KURZ\VisitNet\PublicService\Structs\EmployeeData
     */
    public function setDepartment($department = null)
    {
        if (is_null($department) || (is_array($department) && empty($department))) {
            unset($this->Department);
        } else {
            $this->Department = $department;
        }
        return $this;
    }
    /**
     * Get Division value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDivision()
    {
        return isset($this->Division) ? $this->Division : null;
    }
    /**
     * Set Division value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $division
     * @return \KURZ\VisitNet\PublicService\Structs\EmployeeData
     */
    public function setDivision($division = null)
    {
        if (is_null($division) || (is_array($division) && empty($division))) {
            unset($this->Division);
        } else {
            $this->Division = $division;
        }
        return $this;
    }
    /**
     * Get OfficePhone value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOfficePhone()
    {
        return isset($this->OfficePhone) ? $this->OfficePhone : null;
    }
    /**
     * Set OfficePhone value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $officePhone
     * @return \KURZ\VisitNet\PublicService\Structs\EmployeeData
     */
    public function setOfficePhone($officePhone = null)
    {
        if (is_null($officePhone) || (is_array($officePhone) && empty($officePhone))) {
            unset($this->OfficePhone);
        } else {
            $this->OfficePhone = $officePhone;
        }
        return $this;
    }
    /**
     * Get OrganisationalUnitName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrganisationalUnitName()
    {
        return isset($this->OrganisationalUnitName) ? $this->OrganisationalUnitName : null;
    }
    /**
     * Set OrganisationalUnitName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $organisationalUnitName
     * @return \KURZ\VisitNet\PublicService\Structs\EmployeeData
     */
    public function setOrganisationalUnitName($organisationalUnitName = null)
    {
        if (is_null($organisationalUnitName) || (is_array($organisationalUnitName) && empty($organisationalUnitName))) {
            unset($this->OrganisationalUnitName);
        } else {
            $this->OrganisationalUnitName = $organisationalUnitName;
        }
        return $this;
    }
    /**
     * Get Room value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRoom()
    {
        return isset($this->Room) ? $this->Room : null;
    }
    /**
     * Set Room value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $room
     * @return \KURZ\VisitNet\PublicService\Structs\EmployeeData
     */
    public function setRoom($room = null)
    {
        if (is_null($room) || (is_array($room) && empty($room))) {
            unset($this->Room);
        } else {
            $this->Room = $room;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\EmployeeData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
