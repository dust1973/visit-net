<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for PersonData Structs
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:PersonData
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class PersonData
{
    /**
     * The EMailAddress
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $EMailAddress;
    /**
     * The FirstName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $FirstName;
    /**
     * The GenderType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $GenderType;
    /**
     * The LastName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $LastName;
    /**
     * The Title
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Title;
    /**
     * The Id
     * Meta information extracted from the WSDL
     * - ref: ser:Id
     * @var string
     */
    public $Id;
    /**
     * The Ref
     * Meta information extracted from the WSDL
     * - ref: ser:Ref
     * @var string
     */
    public $Ref;
    /**
     * Constructor method for PersonData
     * @uses PersonData::setEMailAddress()
     * @uses PersonData::setFirstName()
     * @uses PersonData::setGenderType()
     * @uses PersonData::setLastName()
     * @uses PersonData::setTitle()
     * @uses PersonData::setId()
     * @uses PersonData::setRef()
     * @param string $eMailAddress
     * @param string $firstName
     * @param string $genderType
     * @param string $lastName
     * @param string $title
     * @param string $id
     * @param string $ref
     */
    public function __construct($eMailAddress = null, $firstName = null, $genderType = null, $lastName = null, $title = null, $id = null, $ref = null)
    {
        $this
            ->setEMailAddress($eMailAddress)
            ->setFirstName($firstName)
            ->setGenderType($genderType)
            ->setLastName($lastName)
            ->setTitle($title)
            ->setId($id)
            ->setRef($ref);
    }
    /**
     * Get EMailAddress value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEMailAddress()
    {
        return isset($this->EMailAddress) ? $this->EMailAddress : null;
    }
    /**
     * Set EMailAddress value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $eMailAddress
     * @return \KURZ\VisitNet\PublicService\Structs\PersonData
     */
    public function setEMailAddress($eMailAddress = null)
    {
        if (is_null($eMailAddress) || (is_array($eMailAddress) && empty($eMailAddress))) {
            unset($this->EMailAddress);
        } else {
            $this->EMailAddress = $eMailAddress;
        }
        return $this;
    }
    /**
     * Get FirstName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFirstName()
    {
        return isset($this->FirstName) ? $this->FirstName : null;
    }
    /**
     * Set FirstName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $firstName
     * @return \KURZ\VisitNet\PublicService\Structs\PersonData
     */
    public function setFirstName($firstName = null)
    {
        if (is_null($firstName) || (is_array($firstName) && empty($firstName))) {
            unset($this->FirstName);
        } else {
            $this->FirstName = $firstName;
        }
        return $this;
    }
    /**
     * Get GenderType value
     * @return string|null
     */
    public function getGenderType()
    {
        return $this->GenderType;
    }
    /**
     * Set GenderType value
     * @param string $genderType
     * @return \KURZ\VisitNet\PublicService\Structs\PersonData
     */
    public function setGenderType($genderType = null)
    {
        $this->GenderType = $genderType;
        return $this;
    }
    /**
     * Get LastName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLastName()
    {
        return isset($this->LastName) ? $this->LastName : null;
    }
    /**
     * Set LastName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $lastName
     * @return \KURZ\VisitNet\PublicService\Structs\PersonData
     */
    public function setLastName($lastName = null)
    {
        if (is_null($lastName) || (is_array($lastName) && empty($lastName))) {
            unset($this->LastName);
        } else {
            $this->LastName = $lastName;
        }
        return $this;
    }
    /**
     * Get Title value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTitle()
    {
        return isset($this->Title) ? $this->Title : null;
    }
    /**
     * Set Title value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $title
     * @return \KURZ\VisitNet\PublicService\Structs\PersonData
     */
    public function setTitle($title = null)
    {
        if (is_null($title) || (is_array($title) && empty($title))) {
            unset($this->Title);
        } else {
            $this->Title = $title;
        }
        return $this;
    }
    /**
     * Get Id value
     * @return string|null
     */
    public function getId()
    {
        return $this->Id;
    }
    /**
     * Set Id value
     * @param string $id
     * @return \KURZ\VisitNet\PublicService\Structs\PersonData
     */
    public function setId($id = null)
    {
        $this->Id = $id;
        return $this;
    }
    /**
     * Get Ref value
     * @return string|null
     */
    public function getRef()
    {
        return $this->Ref;
    }
    /**
     * Set Ref value
     * @param string $ref
     * @return \KURZ\VisitNet\PublicService\Structs\PersonData
     */
    public function setRef($ref = null)
    {
        $this->Ref = $ref;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\PersonData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
