<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for IsScheduledExitTimeExceeded Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class IsScheduledExitTimeExceeded
{
    /**
     * The codeValue
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $codeValue;
    /**
     * Constructor method for IsScheduledExitTimeExceeded
     * @uses IsScheduledExitTimeExceeded::setCodeValue()
     * @param string $codeValue
     */
    public function __construct($codeValue = null)
    {
        $this
            ->setCodeValue($codeValue);
    }
    /**
     * Get codeValue value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCodeValue()
    {
        return isset($this->codeValue) ? $this->codeValue : null;
    }
    /**
     * Set codeValue value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $codeValue
     * @return \KURZ\VisitNet\PublicService\Structs\IsScheduledExitTimeExceeded
     */
    public function setCodeValue($codeValue = null)
    {
        if (is_null($codeValue) || (is_array($codeValue) && empty($codeValue))) {
            unset($this->codeValue);
        } else {
            $this->codeValue = $codeValue;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\IsScheduledExitTimeExceeded
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
