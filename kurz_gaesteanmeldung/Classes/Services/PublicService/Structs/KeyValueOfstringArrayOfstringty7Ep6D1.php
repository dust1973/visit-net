<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for KeyValueOfstringArrayOfstringty7Ep6D1 Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class KeyValueOfstringArrayOfstringty7Ep6D1
{
    /**
     * The Key
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Key;
    /**
     * The Value
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfstring
     */
    public $Value;
    /**
     * Constructor method for KeyValueOfstringArrayOfstringty7Ep6D1
     * @uses KeyValueOfstringArrayOfstringty7Ep6D1::setKey()
     * @uses KeyValueOfstringArrayOfstringty7Ep6D1::setValue()
     * @param string $key
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfstring $value
     */
    public function __construct($key = null, \KURZ\VisitNet\PublicService\Arrays\ArrayOfstring $value = null)
    {
        $this
            ->setKey($key)
            ->setValue($value);
    }
    /**
     * Get Key value
     * @return string|null
     */
    public function getKey()
    {
        return $this->Key;
    }
    /**
     * Set Key value
     * @param string $key
     * @return \KURZ\VisitNet\PublicService\Structs\KeyValueOfstringArrayOfstringty7Ep6D1
     */
    public function setKey($key = null)
    {
        $this->Key = $key;
        return $this;
    }
    /**
     * Get Value value
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfstring|null
     */
    public function getValue()
    {
        return $this->Value;
    }
    /**
     * Set Value value
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfstring $value
     * @return \KURZ\VisitNet\PublicService\Structs\KeyValueOfstringArrayOfstringty7Ep6D1
     */
    public function setValue(\KURZ\VisitNet\PublicService\Arrays\ArrayOfstring $value = null)
    {
        $this->Value = $value;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\KeyValueOfstringArrayOfstringty7Ep6D1
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
