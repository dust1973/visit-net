<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for GetVisitDataByIDCard Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class GetVisitDataByIDCard
{
    /**
     * The idCardNameOrGuid
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $idCardNameOrGuid;
    /**
     * The locationName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $locationName;
    /**
     * Constructor method for GetVisitDataByIDCard
     * @uses GetVisitDataByIDCard::setIdCardNameOrGuid()
     * @uses GetVisitDataByIDCard::setLocationName()
     * @param string $idCardNameOrGuid
     * @param string $locationName
     */
    public function __construct($idCardNameOrGuid = null, $locationName = null)
    {
        $this
            ->setIdCardNameOrGuid($idCardNameOrGuid)
            ->setLocationName($locationName);
    }
    /**
     * Get idCardNameOrGuid value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getIdCardNameOrGuid()
    {
        return isset($this->idCardNameOrGuid) ? $this->idCardNameOrGuid : null;
    }
    /**
     * Set idCardNameOrGuid value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $idCardNameOrGuid
     * @return \KURZ\VisitNet\PublicService\Structs\GetVisitDataByIDCard
     */
    public function setIdCardNameOrGuid($idCardNameOrGuid = null)
    {
        if (is_null($idCardNameOrGuid) || (is_array($idCardNameOrGuid) && empty($idCardNameOrGuid))) {
            unset($this->idCardNameOrGuid);
        } else {
            $this->idCardNameOrGuid = $idCardNameOrGuid;
        }
        return $this;
    }
    /**
     * Get locationName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLocationName()
    {
        return isset($this->locationName) ? $this->locationName : null;
    }
    /**
     * Set locationName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $locationName
     * @return \KURZ\VisitNet\PublicService\Structs\GetVisitDataByIDCard
     */
    public function setLocationName($locationName = null)
    {
        if (is_null($locationName) || (is_array($locationName) && empty($locationName))) {
            unset($this->locationName);
        } else {
            $this->locationName = $locationName;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\GetVisitDataByIDCard
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
