<?php

namespace KURZ\VisitNet\PublicService;

/**
 * This class stands for AutomobileType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:AutomobileType
 * @subpackage Enumerations
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class AutomobileType
{
    /**
     * Constant for value 'NotSpecified'
     * @return string 'NotSpecified'
     */
    const VALUE_NOT_SPECIFIED = 'NotSpecified';
    /**
     * Constant for value 'Trailer'
     * @return string 'Trailer'
     */
    const VALUE_TRAILER = 'Trailer';
    /**
     * Constant for value 'Lorry'
     * @return string 'Lorry'
     */
    const VALUE_LORRY = 'Lorry';
    /**
     * Constant for value 'Other'
     * @return string 'Other'
     */
    const VALUE_OTHER = 'Other';
    /**
     * Constant for value 'Car'
     * @return string 'Car'
     */
    const VALUE_CAR = 'Car';
    /**
     * Constant for value 'Van'
     * @return string 'Van'
     */
    const VALUE_VAN = 'Van';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_NOT_SPECIFIED
     * @uses self::VALUE_TRAILER
     * @uses self::VALUE_LORRY
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_CAR
     * @uses self::VALUE_VAN
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_NOT_SPECIFIED,
            self::VALUE_TRAILER,
            self::VALUE_LORRY,
            self::VALUE_OTHER,
            self::VALUE_CAR,
            self::VALUE_VAN,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
