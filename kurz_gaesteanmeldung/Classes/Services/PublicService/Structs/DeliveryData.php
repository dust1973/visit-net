<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for DeliveryData Structs
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:DeliveryData
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class DeliveryData
{
    /**
     * The Appointment
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Appointment;
    /**
     * The BookingNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BookingNumber;
    /**
     * The Material
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Material;
    /**
     * The OrderNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $OrderNumber;
    /**
     * The Pallets
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $Pallets;
    /**
     * The ReferenceNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ReferenceNumber;
    /**
     * The Supplier
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\EnterpriseData
     */
    public $Supplier;
    /**
     * Constructor method for DeliveryData
     * @uses DeliveryData::setAppointment()
     * @uses DeliveryData::setBookingNumber()
     * @uses DeliveryData::setMaterial()
     * @uses DeliveryData::setOrderNumber()
     * @uses DeliveryData::setPallets()
     * @uses DeliveryData::setReferenceNumber()
     * @uses DeliveryData::setSupplier()
     * @param string $appointment
     * @param string $bookingNumber
     * @param string $material
     * @param string $orderNumber
     * @param int $pallets
     * @param string $referenceNumber
     * @param \KURZ\VisitNet\PublicService\Structs\EnterpriseData $supplier
     */
    public function __construct($appointment = null, $bookingNumber = null, $material = null, $orderNumber = null, $pallets = null, $referenceNumber = null, \KURZ\VisitNet\PublicService\Structs\EnterpriseData $supplier = null)
    {
        $this
            ->setAppointment($appointment)
            ->setBookingNumber($bookingNumber)
            ->setMaterial($material)
            ->setOrderNumber($orderNumber)
            ->setPallets($pallets)
            ->setReferenceNumber($referenceNumber)
            ->setSupplier($supplier);
    }
    /**
     * Get Appointment value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAppointment()
    {
        return isset($this->Appointment) ? $this->Appointment : null;
    }
    /**
     * Set Appointment value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $appointment
     * @return \KURZ\VisitNet\PublicService\Structs\DeliveryData
     */
    public function setAppointment($appointment = null)
    {
        if (is_null($appointment) || (is_array($appointment) && empty($appointment))) {
            unset($this->Appointment);
        } else {
            $this->Appointment = $appointment;
        }
        return $this;
    }
    /**
     * Get BookingNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBookingNumber()
    {
        return isset($this->BookingNumber) ? $this->BookingNumber : null;
    }
    /**
     * Set BookingNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $bookingNumber
     * @return \KURZ\VisitNet\PublicService\Structs\DeliveryData
     */
    public function setBookingNumber($bookingNumber = null)
    {
        if (is_null($bookingNumber) || (is_array($bookingNumber) && empty($bookingNumber))) {
            unset($this->BookingNumber);
        } else {
            $this->BookingNumber = $bookingNumber;
        }
        return $this;
    }
    /**
     * Get Material value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMaterial()
    {
        return isset($this->Material) ? $this->Material : null;
    }
    /**
     * Set Material value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $material
     * @return \KURZ\VisitNet\PublicService\Structs\DeliveryData
     */
    public function setMaterial($material = null)
    {
        if (is_null($material) || (is_array($material) && empty($material))) {
            unset($this->Material);
        } else {
            $this->Material = $material;
        }
        return $this;
    }
    /**
     * Get OrderNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrderNumber()
    {
        return isset($this->OrderNumber) ? $this->OrderNumber : null;
    }
    /**
     * Set OrderNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $orderNumber
     * @return \KURZ\VisitNet\PublicService\Structs\DeliveryData
     */
    public function setOrderNumber($orderNumber = null)
    {
        if (is_null($orderNumber) || (is_array($orderNumber) && empty($orderNumber))) {
            unset($this->OrderNumber);
        } else {
            $this->OrderNumber = $orderNumber;
        }
        return $this;
    }
    /**
     * Get Pallets value
     * @return int|null
     */
    public function getPallets()
    {
        return $this->Pallets;
    }
    /**
     * Set Pallets value
     * @param int $pallets
     * @return \KURZ\VisitNet\PublicService\Structs\DeliveryData
     */
    public function setPallets($pallets = null)
    {
        $this->Pallets = $pallets;
        return $this;
    }
    /**
     * Get ReferenceNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReferenceNumber()
    {
        return isset($this->ReferenceNumber) ? $this->ReferenceNumber : null;
    }
    /**
     * Set ReferenceNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $referenceNumber
     * @return \KURZ\VisitNet\PublicService\Structs\DeliveryData
     */
    public function setReferenceNumber($referenceNumber = null)
    {
        if (is_null($referenceNumber) || (is_array($referenceNumber) && empty($referenceNumber))) {
            unset($this->ReferenceNumber);
        } else {
            $this->ReferenceNumber = $referenceNumber;
        }
        return $this;
    }
    /**
     * Get Supplier value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\EnterpriseData|null
     */
    public function getSupplier()
    {
        return isset($this->Supplier) ? $this->Supplier : null;
    }
    /**
     * Set Supplier value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\EnterpriseData $supplier
     * @return \KURZ\VisitNet\PublicService\Structs\DeliveryData
     */
    public function setSupplier(\KURZ\VisitNet\PublicService\Structs\EnterpriseData $supplier = null)
    {
        if (is_null($supplier) || (is_array($supplier) && empty($supplier))) {
            unset($this->Supplier);
        } else {
            $this->Supplier = $supplier;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\DeliveryData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
