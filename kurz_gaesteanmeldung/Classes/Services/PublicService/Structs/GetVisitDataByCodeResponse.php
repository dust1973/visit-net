<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for GetVisitDataByCodeResponse Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class GetVisitDataByCodeResponse
{
    /**
     * The GetVisitDataByCodeResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\VisitData
     */
    public $GetVisitDataByCodeResult;
    /**
     * Constructor method for GetVisitDataByCodeResponse
     * @uses GetVisitDataByCodeResponse::setGetVisitDataByCodeResult()
     * @param \KURZ\VisitNet\PublicService\Structs\VisitData $getVisitDataByCodeResult
     */
    public function __construct(\KURZ\VisitNet\PublicService\Structs\VisitData $getVisitDataByCodeResult = null)
    {
        $this
            ->setGetVisitDataByCodeResult($getVisitDataByCodeResult);
    }
    /**
     * Get GetVisitDataByCodeResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\VisitData|null
     */
    public function getGetVisitDataByCodeResult()
    {
        return isset($this->GetVisitDataByCodeResult) ? $this->GetVisitDataByCodeResult : null;
    }
    /**
     * Set GetVisitDataByCodeResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\VisitData $getVisitDataByCodeResult
     * @return \KURZ\VisitNet\PublicService\Structs\GetVisitDataByCodeResponse
     */
    public function setGetVisitDataByCodeResult(\KURZ\VisitNet\PublicService\Structs\VisitData $getVisitDataByCodeResult = null)
    {
        if (is_null($getVisitDataByCodeResult) || (is_array($getVisitDataByCodeResult) && empty($getVisitDataByCodeResult))) {
            unset($this->GetVisitDataByCodeResult);
        } else {
            $this->GetVisitDataByCodeResult = $getVisitDataByCodeResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\GetVisitDataByCodeResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
