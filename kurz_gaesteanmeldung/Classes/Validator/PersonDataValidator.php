<?php

namespace KURZ\KurzGaesteanmeldung\Validator;


use TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator;
use TYPO3\CMS\Extbase\Validation\Validator\GenericObjectValidator;
use TYPO3\CMS\Extbase\Validation\Validator\ObjectValidatorInterface;
use TYPO3\CMS\Extbase\Validation\Validator\ValidatorInterface;

class PersonDataValidator extends GenericObjectValidator
{

    /**
     * Model that gets validated currently
     *
     * @var \KURZ\KurzGaesteanmeldung\Domain\Model\PersonData|\KURZ\KurzGaesteanmeldung\Domain\Model\EmployeeData|\KURZ\KurzGaesteanmeldung\Domain\Model\GuestData
     */
    protected $model;

    /**
     * Checks if the given value is valid according to the property validators.
     *
     * @param mixed $object The value that should be validated
     */
    protected function isValid($object)
    {
        //$object = $object->GuestData;

/*        if($object instanceof \KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData){
            foreach ($object->GuestData as $object1){
                $this->model = $object1;
                foreach ($this->propertyValidators as $propertyName => $validators) {
                    $propertyValue = $this->getPropertyValue($object1, $propertyName);
                    $this->checkProperty($propertyValue, $validators, $propertyName);
                }
            }
        }else{*/
            $this->model = $object;
            foreach ($this->propertyValidators as $propertyName => $validators) {
                $propertyValue = $this->getPropertyValue($object, $propertyName);
                $this->checkProperty($propertyValue, $validators, $propertyName);
            }
//        }

    }

}