<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for BinaryFile Structs
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:BinaryFile
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class BinaryFile
{
    /**
     * The FileContent
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $FileContent;
    /**
     * The FileName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $FileName;
    /**
     * The FileType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $FileType;
    /**
     * The GuestId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $GuestId;
    /**
     * Constructor method for BinaryFile
     * @uses BinaryFile::setFileContent()
     * @uses BinaryFile::setFileName()
     * @uses BinaryFile::setFileType()
     * @uses BinaryFile::setGuestId()
     * @param string $fileContent
     * @param string $fileName
     * @param string $fileType
     * @param int $guestId
     */
    public function __construct($fileContent = null, $fileName = null, $fileType = null, $guestId = null)
    {
        $this
            ->setFileContent($fileContent)
            ->setFileName($fileName)
            ->setFileType($fileType)
            ->setGuestId($guestId);
    }
    /**
     * Get FileContent value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFileContent()
    {
        return isset($this->FileContent) ? $this->FileContent : null;
    }
    /**
     * Set FileContent value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $fileContent
     * @return \KURZ\VisitNet\PublicService\Structs\BinaryFile
     */
    public function setFileContent($fileContent = null)
    {
        if (is_null($fileContent) || (is_array($fileContent) && empty($fileContent))) {
            unset($this->FileContent);
        } else {
            $this->FileContent = $fileContent;
        }
        return $this;
    }
    /**
     * Get FileName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFileName()
    {
        return isset($this->FileName) ? $this->FileName : null;
    }
    /**
     * Set FileName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $fileName
     * @return \KURZ\VisitNet\PublicService\Structs\BinaryFile
     */
    public function setFileName($fileName = null)
    {
        if (is_null($fileName) || (is_array($fileName) && empty($fileName))) {
            unset($this->FileName);
        } else {
            $this->FileName = $fileName;
        }
        return $this;
    }
    /**
     * Get FileType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFileType()
    {
        return isset($this->FileType) ? $this->FileType : null;
    }
    /**
     * Set FileType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $fileType
     * @return \KURZ\VisitNet\PublicService\Structs\BinaryFile
     */
    public function setFileType($fileType = null)
    {
        if (is_null($fileType) || (is_array($fileType) && empty($fileType))) {
            unset($this->FileType);
        } else {
            $this->FileType = $fileType;
        }
        return $this;
    }
    /**
     * Get GuestId value
     * @return int|null
     */
    public function getGuestId()
    {
        return $this->GuestId;
    }
    /**
     * Set GuestId value
     * @param int $guestId
     * @return \KURZ\VisitNet\PublicService\Structs\BinaryFile
     */
    public function setGuestId($guestId = null)
    {
        $this->GuestId = $guestId;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\BinaryFile
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
