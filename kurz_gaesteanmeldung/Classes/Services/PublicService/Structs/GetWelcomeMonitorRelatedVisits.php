<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for GetWelcomeMonitorRelatedVisits Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class GetWelcomeMonitorRelatedVisits
{
    /**
     * The locationName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $locationName;
    /**
     * The from
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $from;
    /**
     * The to
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $to;
    /**
     * Constructor method for GetWelcomeMonitorRelatedVisits
     * @uses GetWelcomeMonitorRelatedVisits::setLocationName()
     * @uses GetWelcomeMonitorRelatedVisits::setFrom()
     * @uses GetWelcomeMonitorRelatedVisits::setTo()
     * @param string $locationName
     * @param string $from
     * @param string $to
     */
    public function __construct($locationName = null, $from = null, $to = null)
    {
        $this
            ->setLocationName($locationName)
            ->setFrom($from)
            ->setTo($to);
    }
    /**
     * Get locationName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLocationName()
    {
        return isset($this->locationName) ? $this->locationName : null;
    }
    /**
     * Set locationName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $locationName
     * @return \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisits
     */
    public function setLocationName($locationName = null)
    {
        if (is_null($locationName) || (is_array($locationName) && empty($locationName))) {
            unset($this->locationName);
        } else {
            $this->locationName = $locationName;
        }
        return $this;
    }
    /**
     * Get from value
     * @return string|null
     */
    public function getFrom()
    {
        return $this->from;
    }
    /**
     * Set from value
     * @param string $from
     * @return \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisits
     */
    public function setFrom($from = null)
    {
        $this->from = $from;
        return $this;
    }
    /**
     * Get to value
     * @return string|null
     */
    public function getTo()
    {
        return $this->to;
    }
    /**
     * Set to value
     * @param string $to
     * @return \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisits
     */
    public function setTo($to = null)
    {
        $this->to = $to;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisits
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
