<?php


namespace KURZ\KurzGaesteanmeldung\Validator;


use TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator;

class DateTimeValidator extends AbstractValidator
{
    /**
     * Checks if the given value is a valid DateTime object. If this is not
     * the case, the function adds an error.
     *
     * @param mixed $value The value that should be validated
     */
    public function isValid($value)
    {
        $this->result->clear();
        if ($value instanceof \DateTimeInterface) {
            return;
        }
        $this->addError(
            $this->translateErrorMessage(
                'validator.datetime.notvalid',
                'extbase',
                [
                    gettype($value)
                ]
            ),
            1238087674,
            [gettype($value)]
        );
    }
}