<?php

namespace KURZ\KurzGaesteanmeldung\Domain\Model;

/**
 * This class stands for BinaryFile Structs
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:BinaryFile
 * @subpackage Structs
 * @date January 29, 2019, 1:29 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class BinaryFile extends \KURZ\VisitNet\PublicService\Structs\BinaryFile
{

}
