<?php

namespace KURZ\VisitNet\PublicService\Services;

use SoapClient;

/**
 * This class stands for Is Services
 * @subpackage Services
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class Is extends SoapClient
{
    /**
     * Method to call the operation originally named IsValidVisit
     * @uses SoapClient::getSoapClient()
     * @uses SoapClient::setResult()
     * @uses SoapClient::getResult()
     * @uses SoapClient::saveLastError()
     * @param \KURZ\VisitNet\PublicService\Structs\IsValidVisit $parameters
     * @return \KURZ\VisitNet\PublicService\Structs\IsValidVisitResponse|bool
     */
    public function IsValidVisit(\KURZ\VisitNet\PublicService\Structs\IsValidVisit $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->IsValidVisit($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named IsScheduledExitTimeExceeded
     * @uses SoapClient::getSoapClient()
     * @uses SoapClient::setResult()
     * @uses SoapClient::getResult()
     * @uses SoapClient::saveLastError()
     * @param \KURZ\VisitNet\PublicService\Structs\IsScheduledExitTimeExceeded $parameters
     * @return \KURZ\VisitNet\PublicService\Structs\IsScheduledExitTimeExceededResponse|bool
     */
    public function IsScheduledExitTimeExceeded(\KURZ\VisitNet\PublicService\Structs\IsScheduledExitTimeExceeded $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->IsScheduledExitTimeExceeded($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see SoapClient::getResult()
     * @return \KURZ\VisitNet\PublicService\Structs\IsScheduledExitTimeExceededResponse|\KURZ\VisitNet\PublicService\Structs\IsValidVisitResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
