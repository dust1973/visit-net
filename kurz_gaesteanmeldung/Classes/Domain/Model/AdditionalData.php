<?php


namespace KURZ\KurzGaesteanmeldung\Domain\Model;

/***
 *
 * This file is part of the "Guest registration form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 *
 ***/

/*
 * AdditionalData
 */

class AdditionalData
{


    /**
     * relatedVisit
     * @var string
     */
    protected $relatedVisit;

    /**
     * isParking
     * @validate NotEmpty
     * @var boolean
     */
    protected $isParking;


    /**
     * isWifi
     * @validate NotEmpty
     * @var boolean
     */
    protected $isWifi;

    /**
     * isProduktionsGuidedTour
     * @validate NotEmpty
     * @var boolean
     */
    protected $isProduktionsGuidedTour;

    /**
     * isMeetingRoom
     * @validate NotEmpty
     * @var boolean
     */
    protected $isMeetingRoom;


    /**
     * miscellaneous
     *
     * @var string
     */
    protected $miscellaneous = null;

    /**
     * @return string
     */
    public function getRelatedVisit()
    {
        return $this->relatedVisit;
    }

    /**
     * @param string $relatedVisit
     */
    public function setRelatedVisit(string $relatedVisit)
    {
        $this->relatedVisit = $relatedVisit;
    }

    /**
     * @return bool
     */
    public function isParking(): bool
    {
        return $this->isParking;
    }

    /**
     * @param bool $isParking
     */
    public function setIsParking(bool $isParking)
    {
        $this->isParking = $isParking;
    }

    /**
     * @return bool
     */
    public function isWifi(): bool
    {
        return $this->isWifi;
    }

    /**
     * @param bool $isWifi
     */
    public function setIsWifi(bool $isWifi)
    {
        $this->isWifi = $isWifi;
    }

    /**
     * @return bool
     */
    public function isProduktionsGuidedTour()
    {
        return $this->isProduktionsGuidedTour;
    }

    /**
     * @param bool $isProduktionsGuidedTour
     */
    public function setIsProduktionsGuidedTour(bool $isProduktionsGuidedTour)
    {
        $this->isProduktionsGuidedTour = $isProduktionsGuidedTour;
    }

    /**
     * @return bool
     */
    public function isMeetingRoom()
    {
        return $this->isMeetingRoom;
    }

    /**
     * @param bool $isMeetingRoom
     */
    public function setIsMeetingRoom(bool $isMeetingRoom)
    {
        $this->isMeetingRoom = $isMeetingRoom;
    }

    /**
     * @return string
     */
    public function getMiscellaneous()
    {
        return $this->miscellaneous;
    }

    /**
     * @param string $miscellaneous
     */
    public function setMiscellaneous(string $miscellaneous)
    {
        $this->miscellaneous = $miscellaneous;
    }



}