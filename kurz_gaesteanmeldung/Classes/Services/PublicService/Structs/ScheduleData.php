<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for ScheduleData Structs
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ScheduleData
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class ScheduleData
{
    /**
     * The Gate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\GateData
     */
    public $Gate;
    /**
     * The Time
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $Time;
    /**
     * Constructor method for ScheduleData
     * @uses ScheduleData::setGate()
     * @uses ScheduleData::setTime()
     * @param \KURZ\VisitNet\PublicService\Structs\GateData $gate
     * @param string $time
     */
    public function __construct(\KURZ\VisitNet\PublicService\Structs\GateData $gate = null, $time = null)
    {
        $this
            ->setGate($gate)
            ->setTime($time);
    }
    /**
     * Get Gate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\GateData|null
     */
    public function getGate()
    {
        return isset($this->Gate) ? $this->Gate : null;
    }
    /**
     * Set Gate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\GateData $gate
     * @return \KURZ\VisitNet\PublicService\Structs\ScheduleData
     */
    public function setGate(\KURZ\VisitNet\PublicService\Structs\GateData $gate = null)
    {
        if (is_null($gate) || (is_array($gate) && empty($gate))) {
            unset($this->Gate);
        } else {
            $this->Gate = $gate;
        }
        return $this;
    }
    /**
     * Get Time value
     * @return string|null
     */
    public function getTime()
    {
        return $this->Time;
    }
    /**
     * Set Time value
     * @param string $time
     * @return \KURZ\VisitNet\PublicService\Structs\ScheduleData
     */
    public function setTime($time = null)
    {
        $this->Time = $time;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\ScheduleData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
