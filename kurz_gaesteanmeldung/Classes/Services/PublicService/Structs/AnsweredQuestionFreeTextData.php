<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for AnsweredQuestionFreeTextData Structs
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:AnsweredQuestionFreeTextData
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class AnsweredQuestionFreeTextData extends ExtendedAnsweredQuestionData
{
    /**
     * The AnswerText
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $AnswerText;
    /**
     * Constructor method for AnsweredQuestionFreeTextData
     * @uses AnsweredQuestionFreeTextData::setAnswerText()
     * @param string $answerText
     */
    public function __construct($answerText = null)
    {
        $this
            ->setAnswerText($answerText);
    }
    /**
     * Get AnswerText value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAnswerText()
    {
        return isset($this->AnswerText) ? $this->AnswerText : null;
    }
    /**
     * Set AnswerText value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $answerText
     * @return \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionFreeTextData
     */
    public function setAnswerText($answerText = null)
    {
        if (is_null($answerText) || (is_array($answerText) && empty($answerText))) {
            unset($this->AnswerText);
        } else {
            $this->AnswerText = $answerText;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionFreeTextData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
