<?php
namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for VisitRequestData Structs
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:VisitRequestData
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class VisitRequestData
{
    /**
     * The AdditionalInformationForSecurityStaff
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $AdditionalInformationForSecurityStaff;
    /**
     * The Announcer
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\EmployeeData
     */
    public $Announcer;
    /**
     * The AnonymousVisitors
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $AnonymousVisitors;
    /**
     * The AnsweredQuestions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1
     */
    public $AnsweredQuestions;
    /**
     * The Attachment
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\BinaryFile
     */
    public $Attachment;
    /**
     * The Building
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Building;
    /**
     * The Employee
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\EmployeeData
     */
    public $Employee;
    /**
     * The EmployeeDeputy
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\EmployeeData
     */
    public $EmployeeDeputy;
    /**
     * The LocationName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $LocationName;
    /**
     * The ScheduleTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ScheduleTime;
    /**
     * The ScheduledExitTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ScheduledExitTime;
    /**
     * The ShowOnWelcomeMonitor
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $ShowOnWelcomeMonitor;
    /**
     * The Visitors
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData
     */
    public $Visitors;
    /**
     * Constructor method for VisitRequestData
     * @uses VisitRequestData::setAdditionalInformationForSecurityStaff()
     * @uses VisitRequestData::setAnnouncer()
     * @uses VisitRequestData::setAnonymousVisitors()
     * @uses VisitRequestData::setAnsweredQuestions()
     * @uses VisitRequestData::setAttachment()
     * @uses VisitRequestData::setBuilding()
     * @uses VisitRequestData::setEmployee()
     * @uses VisitRequestData::setEmployeeDeputy()
     * @uses VisitRequestData::setLocationName()
     * @uses VisitRequestData::setScheduleTime()
     * @uses VisitRequestData::setScheduledExitTime()
     * @uses VisitRequestData::setShowOnWelcomeMonitor()
     * @uses VisitRequestData::setVisitors()
     * @param string $additionalInformationForSecurityStaff
     * @param \KURZ\VisitNet\PublicService\Structs\EmployeeData $announcer
     * @param int $anonymousVisitors
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $answeredQuestions
     * @param \KURZ\VisitNet\PublicService\Structs\BinaryFile $attachment
     * @param string $building
     * @param \KURZ\VisitNet\PublicService\Structs\EmployeeData $employee
     * @param \KURZ\VisitNet\PublicService\Structs\EmployeeData $employeeDeputy
     * @param string $locationName
     * @param string $scheduleTime
     * @param string $scheduledExitTime
     * @param bool $showOnWelcomeMonitor
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData $visitors
     */
    public function __construct($additionalInformationForSecurityStaff = null, \KURZ\VisitNet\PublicService\Structs\EmployeeData $announcer = null, $anonymousVisitors = null, \KURZ\VisitNet\PublicService\Arrays\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $answeredQuestions = null, \KURZ\VisitNet\PublicService\Structs\BinaryFile $attachment = null, $building = null, \KURZ\VisitNet\PublicService\Structs\EmployeeData $employee = null, \KURZ\VisitNet\PublicService\Structs\EmployeeData $employeeDeputy = null, $locationName = null, $scheduleTime = null, $scheduledExitTime = null, $showOnWelcomeMonitor = null, \KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData $visitors = null)
    {
        $this
            ->setAdditionalInformationForSecurityStaff($additionalInformationForSecurityStaff)
            ->setAnnouncer($announcer)
            ->setAnonymousVisitors($anonymousVisitors)
            ->setAnsweredQuestions($answeredQuestions)
            ->setAttachment($attachment)
            ->setBuilding($building)
            ->setEmployee($employee)
            ->setEmployeeDeputy($employeeDeputy)
            ->setLocationName($locationName)
            ->setScheduleTime($scheduleTime)
            ->setScheduledExitTime($scheduledExitTime)
            ->setShowOnWelcomeMonitor($showOnWelcomeMonitor)
            ->setVisitors($visitors);
    }
    /**
     * Get AdditionalInformationForSecurityStaff value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdditionalInformationForSecurityStaff()
    {
        return isset($this->AdditionalInformationForSecurityStaff) ? $this->AdditionalInformationForSecurityStaff : null;
    }
    /**
     * Set AdditionalInformationForSecurityStaff value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $additionalInformationForSecurityStaff
     * @return \KURZ\VisitNet\PublicService\Structs\VisitRequestData
     */
    public function setAdditionalInformationForSecurityStaff($additionalInformationForSecurityStaff = null)
    {
        if (is_null($additionalInformationForSecurityStaff) || (is_array($additionalInformationForSecurityStaff) && empty($additionalInformationForSecurityStaff))) {
            unset($this->AdditionalInformationForSecurityStaff);
        } else {
            $this->AdditionalInformationForSecurityStaff = $additionalInformationForSecurityStaff;
        }
        return $this;
    }
    /**
     * Get Announcer value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\EmployeeData|null
     */
    public function getAnnouncer()
    {
        return isset($this->Announcer) ? $this->Announcer : null;
    }
    /**
     * Set Announcer value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\EmployeeData $announcer
     * @return \KURZ\VisitNet\PublicService\Structs\VisitRequestData
     */
    public function setAnnouncer(\KURZ\VisitNet\PublicService\Structs\EmployeeData $announcer = null)
    {
        if (is_null($announcer) || (is_array($announcer) && empty($announcer))) {
            unset($this->Announcer);
        } else {
            $this->Announcer = $announcer;
        }
        return $this;
    }
    /**
     * Get AnonymousVisitors value
     * @return int|null
     */
    public function getAnonymousVisitors()
    {
        return $this->AnonymousVisitors;
    }
    /**
     * Set AnonymousVisitors value
     * @param int $anonymousVisitors
     * @return \KURZ\VisitNet\PublicService\Structs\VisitRequestData
     */
    public function setAnonymousVisitors($anonymousVisitors = null)
    {
        $this->AnonymousVisitors = $anonymousVisitors;
        return $this;
    }
    /**
     * Get AnsweredQuestions value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1|null
     */
    public function getAnsweredQuestions()
    {
        return isset($this->AnsweredQuestions) ? $this->AnsweredQuestions : null;
    }
    /**
     * Set AnsweredQuestions value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $answeredQuestions
     * @return \KURZ\VisitNet\PublicService\Structs\VisitRequestData
     */
    public function setAnsweredQuestions(\KURZ\VisitNet\PublicService\Arrays\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $answeredQuestions = null)
    {
        if (is_null($answeredQuestions) || (is_array($answeredQuestions) && empty($answeredQuestions))) {
            unset($this->AnsweredQuestions);
        } else {
            $this->AnsweredQuestions = $answeredQuestions;
        }
        return $this;
    }
    /**
     * Get Attachment value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\BinaryFile|null
     */
    public function getAttachment()
    {
        return isset($this->Attachment) ? $this->Attachment : null;
    }
    /**
     * Set Attachment value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\BinaryFile $attachment
     * @return \KURZ\VisitNet\PublicService\Structs\VisitRequestData
     */
    public function setAttachment(\KURZ\VisitNet\PublicService\Structs\BinaryFile $attachment = null)
    {
        if (is_null($attachment) || (is_array($attachment) && empty($attachment))) {
            unset($this->Attachment);
        } else {
            $this->Attachment = $attachment;
        }
        return $this;
    }
    /**
     * Get Building value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBuilding()
    {
        return isset($this->Building) ? $this->Building : null;
    }
    /**
     * Set Building value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $building
     * @return \KURZ\VisitNet\PublicService\Structs\VisitRequestData
     */
    public function setBuilding($building = null)
    {
        if (is_null($building) || (is_array($building) && empty($building))) {
            unset($this->Building);
        } else {
            $this->Building = $building;
        }
        return $this;
    }
    /**
     * Get Employee value
     * @return \KURZ\VisitNet\PublicService\Structs\EmployeeData|null
     */
    public function getEmployee()
    {
        return $this->Employee;
    }
    /**
     * Set Employee value
     * @param \KURZ\VisitNet\PublicService\Structs\EmployeeData $employee
     * @return \KURZ\VisitNet\PublicService\Structs\VisitRequestData
     */
    public function setEmployee(\KURZ\VisitNet\PublicService\Structs\EmployeeData $employee = null)
    {
        $this->Employee = $employee;
        return $this;
    }
    /**
     * Get EmployeeDeputy value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\EmployeeData|null
     */
    public function getEmployeeDeputy()
    {
        return isset($this->EmployeeDeputy) ? $this->EmployeeDeputy : null;
    }
    /**
     * Set EmployeeDeputy value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\EmployeeData $employeeDeputy
     * @return \KURZ\VisitNet\PublicService\Structs\VisitRequestData
     */
    public function setEmployeeDeputy(\KURZ\VisitNet\PublicService\Structs\EmployeeData $employeeDeputy = null)
    {
        if (is_null($employeeDeputy) || (is_array($employeeDeputy) && empty($employeeDeputy))) {
            unset($this->EmployeeDeputy);
        } else {
            $this->EmployeeDeputy = $employeeDeputy;
        }
        return $this;
    }
    /**
     * Get LocationName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLocationName()
    {
        return isset($this->LocationName) ? $this->LocationName : null;
    }
    /**
     * Set LocationName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $locationName
     * @return \KURZ\VisitNet\PublicService\Structs\VisitRequestData
     */
    public function setLocationName($locationName = null)
    {
        if (is_null($locationName) || (is_array($locationName) && empty($locationName))) {
            unset($this->LocationName);
        } else {
            $this->LocationName = $locationName;
        }
        return $this;
    }
    /**
     * Get ScheduleTime value
     * @return string|null
     */
    public function getScheduleTime()
    {
        return $this->ScheduleTime;
    }
    /**
     * Set ScheduleTime value
     * @param string $scheduleTime
     * @return \KURZ\VisitNet\PublicService\Structs\VisitRequestData
     */
    public function setScheduleTime($scheduleTime = null)
    {
        $this->ScheduleTime = $scheduleTime;
        return $this;
    }
    /**
     * Get ScheduledExitTime value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getScheduledExitTime()
    {
        return isset($this->ScheduledExitTime) ? $this->ScheduledExitTime : null;
    }
    /**
     * Set ScheduledExitTime value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $scheduledExitTime
     * @return \KURZ\VisitNet\PublicService\Structs\VisitRequestData
     */
    public function setScheduledExitTime($scheduledExitTime = null)
    {
        if (is_null($scheduledExitTime) || (is_array($scheduledExitTime) && empty($scheduledExitTime))) {
            unset($this->ScheduledExitTime);
        } else {
            $this->ScheduledExitTime = $scheduledExitTime;
        }
        return $this;
    }
    /**
     * Get ShowOnWelcomeMonitor value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getShowOnWelcomeMonitor()
    {
        return isset($this->ShowOnWelcomeMonitor) ? $this->ShowOnWelcomeMonitor : null;
    }
    /**
     * Set ShowOnWelcomeMonitor value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $showOnWelcomeMonitor
     * @return \KURZ\VisitNet\PublicService\Structs\VisitRequestData
     */
    public function setShowOnWelcomeMonitor($showOnWelcomeMonitor = null)
    {
        if (is_null($showOnWelcomeMonitor) || (is_array($showOnWelcomeMonitor) && empty($showOnWelcomeMonitor))) {
            unset($this->ShowOnWelcomeMonitor);
        } else {
            $this->ShowOnWelcomeMonitor = $showOnWelcomeMonitor;
        }
        return $this;
    }
    /**
     * Get Visitors value
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData|null
     */
    public function getVisitors()
    {
        return $this->Visitors;
    }
    /**
     * Set Visitors value
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData $visitors
     * @return \KURZ\VisitNet\PublicService\Structs\VisitRequestData
     */
    public function setVisitors(\KURZ\VisitNet\PublicService\Arrays\ArrayOfGuestData $visitors = null)
    {
        $this->Visitors = $visitors;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\VisitRequestData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
