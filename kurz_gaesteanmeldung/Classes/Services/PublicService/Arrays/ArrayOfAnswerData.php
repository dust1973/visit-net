<?php

namespace KURZ\VisitNet\PublicService\Arrays;

/**
 * This class stands for ArrayOfAnswerData Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfAnswerData
 * @subpackage Arrays
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class ArrayOfAnswerData
{
    /**
     * The AnswerData
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\AnswerData[]
     */
    public $AnswerData;
    /**
     * Constructor method for ArrayOfAnswerData
     * @uses ArrayOfAnswerData::setAnswerData()
     * @param \KURZ\VisitNet\PublicService\Structs\AnswerData[] $answerData
     */
    public function __construct(array $answerData = array())
    {
        $this
            ->setAnswerData($answerData);
    }
    /**
     * Get AnswerData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\AnswerData[]|null
     */
    public function getAnswerData()
    {
        return isset($this->AnswerData) ? $this->AnswerData : null;
    }
    /**
     * Set AnswerData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\AnswerData[] $answerData
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnswerData
     */
    public function setAnswerData(array $answerData = array())
    {
        if (is_null($answerData) || (is_array($answerData) && empty($answerData))) {
            unset($this->AnswerData);
        } else {
            $this->AnswerData = $answerData;
        }
        return $this;
    }
    /**
     * Add item to AnswerData value
     * @throws \InvalidArgumentException
     * @param \KURZ\VisitNet\PublicService\Structs\AnswerData $item
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnswerData
     */
    public function addToAnswerData(\KURZ\VisitNet\PublicService\Structs\AnswerData $item)
    {
        $this->AnswerData[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see ::current()
     * @return \KURZ\VisitNet\PublicService\Structs\AnswerData|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see ::item()
     * @param int $index
     * @return \KURZ\VisitNet\PublicService\Structs\AnswerData|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see ::first()
     * @return \KURZ\VisitNet\PublicService\Structs\AnswerData|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see ::last()
     * @return \KURZ\VisitNet\PublicService\Structs\AnswerData|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see ::offsetGet()
     * @param int $offset
     * @return \KURZ\VisitNet\PublicService\Structs\AnswerData|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see ::getAttributeName()
     * @return string AnswerData
     */
    public function getAttributeName()
    {
        return 'AnswerData';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnswerData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
