<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for GuestData Structs
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:GuestData
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class GuestData extends PersonData
{
    /**
     * The Automobile
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\AutomobileData
     */
    public $Automobile;
    /**
     * The Company
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\EnterpriseData
     */
    public $Company;
    /**
     * The DateOfBirth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DateOfBirth;
    /**
     * The LanguageCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $LanguageCode;
    /**
     * The Phone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Phone;
    /**
     * The Picture
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Picture;
    /**
     * Constructor method for GuestData
     * @uses GuestData::setAutomobile()
     * @uses GuestData::setCompany()
     * @uses GuestData::setDateOfBirth()
     * @uses GuestData::setLanguageCode()
     * @uses GuestData::setPhone()
     * @uses GuestData::setPicture()
     * @param \KURZ\VisitNet\PublicService\Structs\AutomobileData $automobile
     * @param \KURZ\VisitNet\PublicService\Structs\EnterpriseData $company
     * @param string $dateOfBirth
     * @param string $languageCode
     * @param string $phone
     * @param string $picture
     */
    public function __construct(\KURZ\VisitNet\PublicService\Structs\AutomobileData $automobile = null, \KURZ\VisitNet\PublicService\Structs\EnterpriseData $company = null, $dateOfBirth = null, $languageCode = null, $phone = null, $picture = null)
    {
        $this
            ->setAutomobile($automobile)
            ->setCompany($company)
            ->setDateOfBirth($dateOfBirth)
            ->setLanguageCode($languageCode)
            ->setPhone($phone)
            ->setPicture($picture);
    }
    /**
     * Get Automobile value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\AutomobileData|null
     */
    public function getAutomobile()
    {
        return isset($this->Automobile) ? $this->Automobile : null;
    }
    /**
     * Set Automobile value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\AutomobileData $automobile
     * @return \KURZ\VisitNet\PublicService\Structs\GuestData
     */
    public function setAutomobile(\KURZ\VisitNet\PublicService\Structs\AutomobileData $automobile = null)
    {
        if (is_null($automobile) || (is_array($automobile) && empty($automobile))) {
            unset($this->Automobile);
        } else {
            $this->Automobile = $automobile;
        }
        return $this;
    }
    /**
     * Get Company value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\EnterpriseData|null
     */
    public function getCompany()
    {
        return isset($this->Company) ? $this->Company : null;
    }
    /**
     * Set Company value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\EnterpriseData $company
     * @return \KURZ\VisitNet\PublicService\Structs\GuestData
     */
    public function setCompany(\KURZ\VisitNet\PublicService\Structs\EnterpriseData $company = null)
    {
        if (is_null($company) || (is_array($company) && empty($company))) {
            unset($this->Company);
        } else {
            $this->Company = $company;
        }
        return $this;
    }
    /**
     * Get DateOfBirth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDateOfBirth()
    {
        return isset($this->DateOfBirth) ? $this->DateOfBirth : null;
    }
    /**
     * Set DateOfBirth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dateOfBirth
     * @return \KURZ\VisitNet\PublicService\Structs\GuestData
     */
    public function setDateOfBirth($dateOfBirth = null)
    {
        if (is_null($dateOfBirth) || (is_array($dateOfBirth) && empty($dateOfBirth))) {
            unset($this->DateOfBirth);
        } else {
            $this->DateOfBirth = $dateOfBirth;
        }
        return $this;
    }
    /**
     * Get LanguageCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLanguageCode()
    {
        return isset($this->LanguageCode) ? $this->LanguageCode : null;
    }
    /**
     * Set LanguageCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $languageCode
     * @return \KURZ\VisitNet\PublicService\Structs\GuestData
     */
    public function setLanguageCode($languageCode = null)
    {
        if (is_null($languageCode) || (is_array($languageCode) && empty($languageCode))) {
            unset($this->LanguageCode);
        } else {
            $this->LanguageCode = $languageCode;
        }
        return $this;
    }
    /**
     * Get Phone value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPhone()
    {
        return isset($this->Phone) ? $this->Phone : null;
    }
    /**
     * Set Phone value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $phone
     * @return \KURZ\VisitNet\PublicService\Structs\GuestData
     */
    public function setPhone($phone = null)
    {
        if (is_null($phone) || (is_array($phone) && empty($phone))) {
            unset($this->Phone);
        } else {
            $this->Phone = $phone;
        }
        return $this;
    }
    /**
     * Get Picture value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPicture()
    {
        return isset($this->Picture) ? $this->Picture : null;
    }
    /**
     * Set Picture value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $picture
     * @return \KURZ\VisitNet\PublicService\Structs\GuestData
     */
    public function setPicture($picture = null)
    {
        if (is_null($picture) || (is_array($picture) && empty($picture))) {
            unset($this->Picture);
        } else {
            $this->Picture = $picture;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\GuestData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
