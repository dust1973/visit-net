<?php

namespace KURZ\VisitNet\PublicService\Arrays;

/**
 * This class stands for ArrayOfAnsweredQuestionData Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfAnsweredQuestionData
 * @subpackage Arrays
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class ArrayOfAnsweredQuestionData
{
    /**
     * The AnsweredQuestionData
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionData[]
     */
    public $AnsweredQuestionData;
    /**
     * Constructor method for ArrayOfAnsweredQuestionData
     * @uses ArrayOfAnsweredQuestionData::setAnsweredQuestionData()
     * @param \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionData[] $answeredQuestionData
     */
    public function __construct(array $answeredQuestionData = array())
    {
        $this
            ->setAnsweredQuestionData($answeredQuestionData);
    }
    /**
     * Get AnsweredQuestionData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionData[]|null
     */
    public function getAnsweredQuestionData()
    {
        return isset($this->AnsweredQuestionData) ? $this->AnsweredQuestionData : null;
    }
    /**
     * Set AnsweredQuestionData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionData[] $answeredQuestionData
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnsweredQuestionData
     */
    public function setAnsweredQuestionData(array $answeredQuestionData = array())
    {
        if (is_null($answeredQuestionData) || (is_array($answeredQuestionData) && empty($answeredQuestionData))) {
            unset($this->AnsweredQuestionData);
        } else {
            $this->AnsweredQuestionData = $answeredQuestionData;
        }
        return $this;
    }
    /**
     * Add item to AnsweredQuestionData value
     * @throws \InvalidArgumentException
     * @param \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionData $item
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnsweredQuestionData
     */
    public function addToAnsweredQuestionData(\KURZ\VisitNet\PublicService\Structs\AnsweredQuestionData $item)
    {
        $this->AnsweredQuestionData[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see ::current()
     * @return \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionData|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see ::item()
     * @param int $index
     * @return \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionData|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see ::first()
     * @return \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionData|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see ::last()
     * @return \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionData|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see ::offsetGet()
     * @param int $offset
     * @return \KURZ\VisitNet\PublicService\Structs\AnsweredQuestionData|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see ::getAttributeName()
     * @return string AnsweredQuestionData
     */
    public function getAttributeName()
    {
        return 'AnsweredQuestionData';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfAnsweredQuestionData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
