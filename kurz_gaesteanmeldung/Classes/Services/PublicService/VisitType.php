<?php

namespace KURZ\VisitNet\PublicService;

/**
 * This class stands for VisitType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:VisitType
 * @subpackage Enumerations
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class VisitType
{
    /**
     * Constant for value 'NotSpecified'
     * @return string 'NotSpecified'
     */
    const VALUE_NOT_SPECIFIED = 'NotSpecified';
    /**
     * Constant for value 'Visitor'
     * @return string 'Visitor'
     */
    const VALUE_VISITOR = 'Visitor';
    /**
     * Constant for value 'Delivery'
     * @return string 'Delivery'
     */
    const VALUE_DELIVERY = 'Delivery';
    /**
     * Constant for value 'Contractor'
     * @return string 'Contractor'
     */
    const VALUE_CONTRACTOR = 'Contractor';
    /**
     * Constant for value 'All'
     * @return string 'All'
     */
    const VALUE_ALL = 'All';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_NOT_SPECIFIED
     * @uses self::VALUE_VISITOR
     * @uses self::VALUE_DELIVERY
     * @uses self::VALUE_CONTRACTOR
     * @uses self::VALUE_ALL
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_NOT_SPECIFIED,
            self::VALUE_VISITOR,
            self::VALUE_DELIVERY,
            self::VALUE_CONTRACTOR,
            self::VALUE_ALL,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
