<?php

namespace KURZ\VisitNet\PublicService\Services;

use SoapClient;

/**
 * This class stands for Get Services
 * @subpackage Services
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class Get extends SoapClient
{
    /**
     * Method to call the operation originally named GetVisitDataByCode
     * @uses SoapClient::getSoapClient()
     * @uses SoapClient::setResult()
     * @uses SoapClient::getResult()
     * @uses SoapClient::saveLastError()
     * @param \KURZ\VisitNet\PublicService\Structs\GetVisitDataByCode $parameters
     * @return \KURZ\VisitNet\PublicService\Structs\GetVisitDataByCodeResponse|bool
     */
    public function GetVisitDataByCode(\KURZ\VisitNet\PublicService\Structs\GetVisitDataByCode $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetVisitDataByCode($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetVisitDataByIDCard
     * @uses SoapClient::getSoapClient()
     * @uses SoapClient::setResult()
     * @uses SoapClient::getResult()
     * @uses SoapClient::saveLastError()
     * @param \KURZ\VisitNet\PublicService\Structs\GetVisitDataByIDCard $parameters
     * @return \KURZ\VisitNet\PublicService\Structs\GetVisitDataByIDCardResponse|bool
     */
    public function GetVisitDataByIDCard(\KURZ\VisitNet\PublicService\Structs\GetVisitDataByIDCard $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetVisitDataByIDCard($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetGuestDataByCode
     * @uses SoapClient::getSoapClient()
     * @uses SoapClient::setResult()
     * @uses SoapClient::getResult()
     * @uses SoapClient::saveLastError()
     * @param \KURZ\VisitNet\PublicService\Structs\GetGuestDataByCode $parameters
     * @return \KURZ\VisitNet\PublicService\Structs\GetGuestDataByCodeResponse|bool
     */
    public function GetGuestDataByCode(\KURZ\VisitNet\PublicService\Structs\GetGuestDataByCode $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetGuestDataByCode($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDeliveryData
     * @uses SoapClient::getSoapClient()
     * @uses SoapClient::setResult()
     * @uses SoapClient::getResult()
     * @uses SoapClient::saveLastError()
     * @param \KURZ\VisitNet\PublicService\Structs\GetDeliveryData $parameters
     * @return \KURZ\VisitNet\PublicService\Structs\GetDeliveryDataResponse|bool
     */
    public function GetDeliveryData(\KURZ\VisitNet\PublicService\Structs\GetDeliveryData $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetDeliveryData($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetWelcomeMonitorRelatedVisits
     * @uses SoapClient::getSoapClient()
     * @uses SoapClient::setResult()
     * @uses SoapClient::getResult()
     * @uses SoapClient::saveLastError()
     * @param \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisits $parameters
     * @return \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisitsResponse|bool
     */
    public function GetWelcomeMonitorRelatedVisits(\KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisits $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetWelcomeMonitorRelatedVisits($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named
     * GetWelcomeMonitorRelatedVisitsInMinutes
     * @uses SoapClient::getSoapClient()
     * @uses SoapClient::setResult()
     * @uses SoapClient::getResult()
     * @uses SoapClient::saveLastError()
     * @param \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisitsInMinutes $parameters
     * @return \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisitsInMinutesResponse|bool
     */
    public function GetWelcomeMonitorRelatedVisitsInMinutes(\KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisitsInMinutes $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetWelcomeMonitorRelatedVisitsInMinutes($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetPersonVisits
     * @uses SoapClient::getSoapClient()
     * @uses SoapClient::setResult()
     * @uses SoapClient::getResult()
     * @uses SoapClient::saveLastError()
     * @param \KURZ\VisitNet\PublicService\Structs\GetPersonVisits $parameters
     * @return \KURZ\VisitNet\PublicService\Structs\GetPersonVisitsResponse|bool
     */
    public function GetPersonVisits(\KURZ\VisitNet\PublicService\Structs\GetPersonVisits $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetPersonVisits($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDeliveryVisits
     * @uses SoapClient::getSoapClient()
     * @uses SoapClient::setResult()
     * @uses SoapClient::getResult()
     * @uses SoapClient::saveLastError()
     * @param \KURZ\VisitNet\PublicService\Structs\GetDeliveryVisits $parameters
     * @return \KURZ\VisitNet\PublicService\Structs\GetDeliveryVisitsResponse|bool
     */
    public function GetDeliveryVisits(\KURZ\VisitNet\PublicService\Structs\GetDeliveryVisits $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetDeliveryVisits($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetContractorVisits
     * @uses SoapClient::getSoapClient()
     * @uses SoapClient::setResult()
     * @uses SoapClient::getResult()
     * @uses SoapClient::saveLastError()
     * @param \KURZ\VisitNet\PublicService\Structs\GetContractorVisits $parameters
     * @return \KURZ\VisitNet\PublicService\Structs\GetContractorVisitsResponse|bool
     */
    public function GetContractorVisits(\KURZ\VisitNet\PublicService\Structs\GetContractorVisits $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetContractorVisits($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see SoapClient::getResult()
     * @return \KURZ\VisitNet\PublicService\Structs\GetContractorVisitsResponse|\KURZ\VisitNet\PublicService\Structs\GetDeliveryDataResponse|\KURZ\VisitNet\PublicService\Structs\GetDeliveryVisitsResponse|\KURZ\VisitNet\PublicService\Structs\GetGuestDataByCodeResponse|\KURZ\VisitNet\PublicService\Structs\GetPersonVisitsResponse|\KURZ\VisitNet\PublicService\Structs\GetVisitDataByCodeResponse|\KURZ\VisitNet\PublicService\Structs\GetVisitDataByIDCardResponse|\KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisitsInMinutesResponse|\KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisitsResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
