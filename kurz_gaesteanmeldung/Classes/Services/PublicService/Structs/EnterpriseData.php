<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for EnterpriseData Structs
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:EnterpriseData
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class EnterpriseData
{
    /**
     * The City
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $City;
    /**
     * The Country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Country;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Name;
    /**
     * The Street
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Street;
    /**
     * The Zip
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Zip;
    /**
     * The Id
     * Meta information extracted from the WSDL
     * - ref: ser:Id
     * @var string
     */
    public $Id;
    /**
     * The Ref
     * Meta information extracted from the WSDL
     * - ref: ser:Ref
     * @var string
     */
    public $Ref;
    /**
     * Constructor method for EnterpriseData
     * @uses EnterpriseData::setCity()
     * @uses EnterpriseData::setCountry()
     * @uses EnterpriseData::setName()
     * @uses EnterpriseData::setStreet()
     * @uses EnterpriseData::setZip()
     * @uses EnterpriseData::setId()
     * @uses EnterpriseData::setRef()
     * @param string $city
     * @param string $country
     * @param string $name
     * @param string $street
     * @param string $zip
     * @param string $id
     * @param string $ref
     */
    public function __construct($city = null, $country = null, $name = null, $street = null, $zip = null, $id = null, $ref = null)
    {
        $this
            ->setCity($city)
            ->setCountry($country)
            ->setName($name)
            ->setStreet($street)
            ->setZip($zip)
            ->setId($id)
            ->setRef($ref);
    }
    /**
     * Get City value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCity()
    {
        return isset($this->City) ? $this->City : null;
    }
    /**
     * Set City value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $city
     * @return \KURZ\VisitNet\PublicService\Structs\EnterpriseData
     */
    public function setCity($city = null)
    {
        if (is_null($city) || (is_array($city) && empty($city))) {
            unset($this->City);
        } else {
            $this->City = $city;
        }
        return $this;
    }
    /**
     * Get Country value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountry()
    {
        return isset($this->Country) ? $this->Country : null;
    }
    /**
     * Set Country value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $country
     * @return \KURZ\VisitNet\PublicService\Structs\EnterpriseData
     */
    public function setCountry($country = null)
    {
        if (is_null($country) || (is_array($country) && empty($country))) {
            unset($this->Country);
        } else {
            $this->Country = $country;
        }
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName()
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \KURZ\VisitNet\PublicService\Structs\EnterpriseData
     */
    public function setName($name = null)
    {
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        return $this;
    }
    /**
     * Get Street value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStreet()
    {
        return isset($this->Street) ? $this->Street : null;
    }
    /**
     * Set Street value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $street
     * @return \KURZ\VisitNet\PublicService\Structs\EnterpriseData
     */
    public function setStreet($street = null)
    {
        if (is_null($street) || (is_array($street) && empty($street))) {
            unset($this->Street);
        } else {
            $this->Street = $street;
        }
        return $this;
    }
    /**
     * Get Zip value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getZip()
    {
        return isset($this->Zip) ? $this->Zip : null;
    }
    /**
     * Set Zip value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $zip
     * @return \KURZ\VisitNet\PublicService\Structs\EnterpriseData
     */
    public function setZip($zip = null)
    {
        if (is_null($zip) || (is_array($zip) && empty($zip))) {
            unset($this->Zip);
        } else {
            $this->Zip = $zip;
        }
        return $this;
    }
    /**
     * Get Id value
     * @return string|null
     */
    public function getId()
    {
        return $this->Id;
    }
    /**
     * Set Id value
     * @param string $id
     * @return \KURZ\VisitNet\PublicService\Structs\EnterpriseData
     */
    public function setId($id = null)
    {
        $this->Id = $id;
        return $this;
    }
    /**
     * Get Ref value
     * @return string|null
     */
    public function getRef()
    {
        return $this->Ref;
    }
    /**
     * Set Ref value
     * @param string $ref
     * @return \KURZ\VisitNet\PublicService\Structs\EnterpriseData
     */
    public function setRef($ref = null)
    {
        $this->Ref = $ref;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\EnterpriseData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
