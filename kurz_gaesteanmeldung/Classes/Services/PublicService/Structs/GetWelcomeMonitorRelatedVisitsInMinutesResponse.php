<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for GetWelcomeMonitorRelatedVisitsInMinutesResponse Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class GetWelcomeMonitorRelatedVisitsInMinutesResponse
{
    /**
     * The GetWelcomeMonitorRelatedVisitsInMinutesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData
     */
    public $GetWelcomeMonitorRelatedVisitsInMinutesResult;
    /**
     * Constructor method for GetWelcomeMonitorRelatedVisitsInMinutesResponse
     * @uses GetWelcomeMonitorRelatedVisitsInMinutesResponse::setGetWelcomeMonitorRelatedVisitsInMinutesResult()
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getWelcomeMonitorRelatedVisitsInMinutesResult
     */
    public function __construct(\KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getWelcomeMonitorRelatedVisitsInMinutesResult = null)
    {
        $this
            ->setGetWelcomeMonitorRelatedVisitsInMinutesResult($getWelcomeMonitorRelatedVisitsInMinutesResult);
    }
    /**
     * Get GetWelcomeMonitorRelatedVisitsInMinutesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData|null
     */
    public function getGetWelcomeMonitorRelatedVisitsInMinutesResult()
    {
        return isset($this->GetWelcomeMonitorRelatedVisitsInMinutesResult) ? $this->GetWelcomeMonitorRelatedVisitsInMinutesResult : null;
    }
    /**
     * Set GetWelcomeMonitorRelatedVisitsInMinutesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getWelcomeMonitorRelatedVisitsInMinutesResult
     * @return \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisitsInMinutesResponse
     */
    public function setGetWelcomeMonitorRelatedVisitsInMinutesResult(\KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getWelcomeMonitorRelatedVisitsInMinutesResult = null)
    {
        if (is_null($getWelcomeMonitorRelatedVisitsInMinutesResult) || (is_array($getWelcomeMonitorRelatedVisitsInMinutesResult) && empty($getWelcomeMonitorRelatedVisitsInMinutesResult))) {
            unset($this->GetWelcomeMonitorRelatedVisitsInMinutesResult);
        } else {
            $this->GetWelcomeMonitorRelatedVisitsInMinutesResult = $getWelcomeMonitorRelatedVisitsInMinutesResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisitsInMinutesResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
