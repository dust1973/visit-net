<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for GetWelcomeMonitorRelatedVisitsInMinutes Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class GetWelcomeMonitorRelatedVisitsInMinutes
{
    /**
     * The locationName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $locationName;
    /**
     * The minutesBefore
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $minutesBefore;
    /**
     * The minutesAfter
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $minutesAfter;
    /**
     * Constructor method for GetWelcomeMonitorRelatedVisitsInMinutes
     * @uses GetWelcomeMonitorRelatedVisitsInMinutes::setLocationName()
     * @uses GetWelcomeMonitorRelatedVisitsInMinutes::setMinutesBefore()
     * @uses GetWelcomeMonitorRelatedVisitsInMinutes::setMinutesAfter()
     * @param string $locationName
     * @param int $minutesBefore
     * @param int $minutesAfter
     */
    public function __construct($locationName = null, $minutesBefore = null, $minutesAfter = null)
    {
        $this
            ->setLocationName($locationName)
            ->setMinutesBefore($minutesBefore)
            ->setMinutesAfter($minutesAfter);
    }
    /**
     * Get locationName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLocationName()
    {
        return isset($this->locationName) ? $this->locationName : null;
    }
    /**
     * Set locationName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $locationName
     * @return \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisitsInMinutes
     */
    public function setLocationName($locationName = null)
    {
        if (is_null($locationName) || (is_array($locationName) && empty($locationName))) {
            unset($this->locationName);
        } else {
            $this->locationName = $locationName;
        }
        return $this;
    }
    /**
     * Get minutesBefore value
     * @return int|null
     */
    public function getMinutesBefore()
    {
        return $this->minutesBefore;
    }
    /**
     * Set minutesBefore value
     * @param int $minutesBefore
     * @return \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisitsInMinutes
     */
    public function setMinutesBefore($minutesBefore = null)
    {
        $this->minutesBefore = $minutesBefore;
        return $this;
    }
    /**
     * Get minutesAfter value
     * @return int|null
     */
    public function getMinutesAfter()
    {
        return $this->minutesAfter;
    }
    /**
     * Set minutesAfter value
     * @param int $minutesAfter
     * @return \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisitsInMinutes
     */
    public function setMinutesAfter($minutesAfter = null)
    {
        $this->minutesAfter = $minutesAfter;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\GetWelcomeMonitorRelatedVisitsInMinutes
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
