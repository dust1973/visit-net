<?php

namespace KURZ\VisitNet\PublicService\Structs;

/**
 * This class stands for GetPersonVisitsResponse Structs
 * @subpackage Structs
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class GetPersonVisitsResponse
{
    /**
     * The GetPersonVisitsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData
     */
    public $GetPersonVisitsResult;
    /**
     * Constructor method for GetPersonVisitsResponse
     * @uses GetPersonVisitsResponse::setGetPersonVisitsResult()
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getPersonVisitsResult
     */
    public function __construct(\KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getPersonVisitsResult = null)
    {
        $this
            ->setGetPersonVisitsResult($getPersonVisitsResult);
    }
    /**
     * Get GetPersonVisitsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData|null
     */
    public function getGetPersonVisitsResult()
    {
        return isset($this->GetPersonVisitsResult) ? $this->GetPersonVisitsResult : null;
    }
    /**
     * Set GetPersonVisitsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getPersonVisitsResult
     * @return \KURZ\VisitNet\PublicService\Structs\GetPersonVisitsResponse
     */
    public function setGetPersonVisitsResult(\KURZ\VisitNet\PublicService\Arrays\ArrayOfVisitData $getPersonVisitsResult = null)
    {
        if (is_null($getPersonVisitsResult) || (is_array($getPersonVisitsResult) && empty($getPersonVisitsResult))) {
            unset($this->GetPersonVisitsResult);
        } else {
            $this->GetPersonVisitsResult = $getPersonVisitsResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Structs\GetPersonVisitsResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
