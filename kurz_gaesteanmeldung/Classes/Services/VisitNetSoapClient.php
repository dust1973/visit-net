<?php
/**
 * Created by IntelliJ IDEA.
 * User: fuchsa
 * Date: 29.01.2019
 * Time: 13:40
 */

namespace KURZ\KurzGaesteanmeldung\Services;

class VisitNetSoapClient
{
    /**
     * @param string $client
     */
    public $client;

    /**
     * @param string $url
     */
    public $url;

    /**
     * @param array $options
     */
    public $options;



    /**
     * @param array $result
     */
    public $result;

    /**
     * SOAP constructor.
     */
    public function __construct($url, $options)
    {
        $this->setUrl($url);
        $this->setOptions($options);
    }

    /**
     * @return mixed
     */
    public function getSoapClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     */
    public function setSoapClient()
    {
        try {
            $this->client = new \SoapClient($this->url, $this->options);
        } catch (\SoapFault $e) {
        }
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param mixed $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function setResult($result)
    {
        $this->result = $result;
    }


}