<?php

namespace KURZ\VisitNet\PublicService\Arrays;

/**
 * This class stands for ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1
 * @subpackage Arrays
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1
{
    /**
     * The KeyValueOfstringArrayOfstringty7Ep6D1
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \KURZ\VisitNet\PublicService\Structs\KeyValueOfstringArrayOfstringty7Ep6D1[]
     */
    public $KeyValueOfstringArrayOfstringty7Ep6D1;
    /**
     * Constructor method for ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1
     * @uses ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1::setKeyValueOfstringArrayOfstringty7Ep6D1()
     * @param \KURZ\VisitNet\PublicService\Structs\KeyValueOfstringArrayOfstringty7Ep6D1[] $keyValueOfstringArrayOfstringty7Ep6D1
     */
    public function __construct(array $keyValueOfstringArrayOfstringty7Ep6D1 = array())
    {
        $this
            ->setKeyValueOfstringArrayOfstringty7Ep6D1($keyValueOfstringArrayOfstringty7Ep6D1);
    }
    /**
     * Get KeyValueOfstringArrayOfstringty7Ep6D1 value
     * @return \KURZ\VisitNet\PublicService\Structs\KeyValueOfstringArrayOfstringty7Ep6D1[]|null
     */
    public function getKeyValueOfstringArrayOfstringty7Ep6D1()
    {
        return $this->KeyValueOfstringArrayOfstringty7Ep6D1;
    }
    /**
     * Set KeyValueOfstringArrayOfstringty7Ep6D1 value
     * @param \KURZ\VisitNet\PublicService\Structs\KeyValueOfstringArrayOfstringty7Ep6D1[] $keyValueOfstringArrayOfstringty7Ep6D1
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1
     */
    public function setKeyValueOfstringArrayOfstringty7Ep6D1(array $keyValueOfstringArrayOfstringty7Ep6D1 = array())
    {
        $this->KeyValueOfstringArrayOfstringty7Ep6D1 = $keyValueOfstringArrayOfstringty7Ep6D1;
        return $this;
    }
    /**
     * Add item to KeyValueOfstringArrayOfstringty7Ep6D1 value
     * @throws \InvalidArgumentException
     * @param \KURZ\VisitNet\PublicService\Structs\KeyValueOfstringArrayOfstringty7Ep6D1 $item
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1
     */
    public function addToKeyValueOfstringArrayOfstringty7Ep6D1(\KURZ\VisitNet\PublicService\Structs\KeyValueOfstringArrayOfstringty7Ep6D1 $item)
    {
        $this->KeyValueOfstringArrayOfstringty7Ep6D1[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see ::current()
     * @return \KURZ\VisitNet\PublicService\Structs\KeyValueOfstringArrayOfstringty7Ep6D1|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see ::item()
     * @param int $index
     * @return \KURZ\VisitNet\PublicService\Structs\KeyValueOfstringArrayOfstringty7Ep6D1|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see ::first()
     * @return \KURZ\VisitNet\PublicService\Structs\KeyValueOfstringArrayOfstringty7Ep6D1|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see ::last()
     * @return \KURZ\VisitNet\PublicService\Structs\KeyValueOfstringArrayOfstringty7Ep6D1|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see ::offsetGet()
     * @param int $offset
     * @return \KURZ\VisitNet\PublicService\Structs\KeyValueOfstringArrayOfstringty7Ep6D1|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see ::getAttributeName()
     * @return string KeyValueOfstringArrayOfstringty7Ep6D1
     */
    public function getAttributeName()
    {
        return 'KeyValueOfstringArrayOfstringty7Ep6D1';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
