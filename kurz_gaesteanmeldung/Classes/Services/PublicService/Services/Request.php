<?php

namespace KURZ\VisitNet\PublicService\Services;

use SoapClient;

/**
 * This class stands for Request Services
 * @subpackage Services
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class Request extends SoapClient
{
    /**
     * Method to call the operation originally named RequestVisit
     * @uses SoapClient::getSoapClient()
     * @uses SoapClient::setResult()
     * @uses SoapClient::getResult()
     * @uses SoapClient::saveLastError()
     * @param \KURZ\VisitNet\PublicService\Structs\RequestVisit $parameters
     * @return \KURZ\VisitNet\PublicService\Structs\RequestVisitResponse|bool
     */
    public function RequestVisit(\KURZ\VisitNet\PublicService\Structs\RequestVisit $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->RequestVisit($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see SoapClient::getResult()
     * @return \KURZ\VisitNet\PublicService\Structs\RequestVisitResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
