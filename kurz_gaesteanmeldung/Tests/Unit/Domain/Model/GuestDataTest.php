<?php
namespace KURZ\KurzGaesteanmeldung\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>
 */
class GuestDataTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \KURZ\KurzGaesteanmeldung\Domain\Model\GuestData
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \KURZ\KurzGaesteanmeldung\Domain\Model\GuestData();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getDateOfBirthReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDateOfBirth()
        );
    }

    /**
     * @test
     */
    public function setDateOfBirthForDateTimeSetsDateOfBirth()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDateOfBirth($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'dateOfBirth',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLanguageCodeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLanguageCode()
        );
    }

    /**
     * @test
     */
    public function setLanguageCodeForStringSetsLanguageCode()
    {
        $this->subject->setLanguageCode('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'languageCode',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPhoneReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getPhone()
        );
    }

    /**
     * @test
     */
    public function setPhoneForStringSetsPhone()
    {
        $this->subject->setPhone('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'phone',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPictureReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getPicture()
        );
    }

    /**
     * @test
     */
    public function setPictureForFileReferenceSetsPicture()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setPicture($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'picture',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAutomobileReturnsInitialValueForEnterpriseData()
    {
        self::assertEquals(
            null,
            $this->subject->getAutomobile()
        );
    }

    /**
     * @test
     */
    public function setAutomobileForEnterpriseDataSetsAutomobile()
    {
        $automobileFixture = new \KURZ\KurzGaesteanmeldung\Domain\Model\EnterpriseData();
        $this->subject->setAutomobile($automobileFixture);

        self::assertAttributeEquals(
            $automobileFixture,
            'automobile',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCompanyReturnsInitialValueForEnterpriseData()
    {
        self::assertEquals(
            null,
            $this->subject->getCompany()
        );
    }

    /**
     * @test
     */
    public function setCompanyForEnterpriseDataSetsCompany()
    {
        $companyFixture = new \KURZ\KurzGaesteanmeldung\Domain\Model\EnterpriseData();
        $this->subject->setCompany($companyFixture);

        self::assertAttributeEquals(
            $companyFixture,
            'company',
            $this->subject
        );
    }
}
