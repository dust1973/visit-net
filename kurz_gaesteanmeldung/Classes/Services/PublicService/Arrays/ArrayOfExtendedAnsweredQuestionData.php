<?php

namespace KURZ\VisitNet\PublicService\Arrays;

/**
 * This class stands for ArrayOfExtendedAnsweredQuestionData Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfExtendedAnsweredQuestionData
 * @subpackage Arrays
 * @date September 27, 2019, 1:22 pm
 * @author Alexander Fuchs <alexander.fuchs@kurz.de>, LEONHARD KURZ Stiftung & Co. KG
 */
class ArrayOfExtendedAnsweredQuestionData
{
    /**
     * The ExtendedAnsweredQuestionData
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \KURZ\VisitNet\PublicService\Structs\ExtendedAnsweredQuestionData[]
     */
    public $ExtendedAnsweredQuestionData;
    /**
     * Constructor method for ArrayOfExtendedAnsweredQuestionData
     * @uses ArrayOfExtendedAnsweredQuestionData::setExtendedAnsweredQuestionData()
     * @param \KURZ\VisitNet\PublicService\Structs\ExtendedAnsweredQuestionData[] $extendedAnsweredQuestionData
     */
    public function __construct(array $extendedAnsweredQuestionData = array())
    {
        $this
            ->setExtendedAnsweredQuestionData($extendedAnsweredQuestionData);
    }
    /**
     * Get ExtendedAnsweredQuestionData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \KURZ\VisitNet\PublicService\Structs\ExtendedAnsweredQuestionData[]|null
     */
    public function getExtendedAnsweredQuestionData()
    {
        return isset($this->ExtendedAnsweredQuestionData) ? $this->ExtendedAnsweredQuestionData : null;
    }
    /**
     * Set ExtendedAnsweredQuestionData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \KURZ\VisitNet\PublicService\Structs\ExtendedAnsweredQuestionData[] $extendedAnsweredQuestionData
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfExtendedAnsweredQuestionData
     */
    public function setExtendedAnsweredQuestionData(array $extendedAnsweredQuestionData = array())
    {
        if (is_null($extendedAnsweredQuestionData) || (is_array($extendedAnsweredQuestionData) && empty($extendedAnsweredQuestionData))) {
            unset($this->ExtendedAnsweredQuestionData);
        } else {
            $this->ExtendedAnsweredQuestionData = $extendedAnsweredQuestionData;
        }
        return $this;
    }
    /**
     * Add item to ExtendedAnsweredQuestionData value
     * @throws \InvalidArgumentException
     * @param \KURZ\VisitNet\PublicService\Structs\ExtendedAnsweredQuestionData $item
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfExtendedAnsweredQuestionData
     */
    public function addToExtendedAnsweredQuestionData(\KURZ\VisitNet\PublicService\Structs\ExtendedAnsweredQuestionData $item)
    {
        $this->ExtendedAnsweredQuestionData[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see ::current()
     * @return \KURZ\VisitNet\PublicService\Structs\ExtendedAnsweredQuestionData|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see ::item()
     * @param int $index
     * @return \KURZ\VisitNet\PublicService\Structs\ExtendedAnsweredQuestionData|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see ::first()
     * @return \KURZ\VisitNet\PublicService\Structs\ExtendedAnsweredQuestionData|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see ::last()
     * @return \KURZ\VisitNet\PublicService\Structs\ExtendedAnsweredQuestionData|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see ::offsetGet()
     * @param int $offset
     * @return \KURZ\VisitNet\PublicService\Structs\ExtendedAnsweredQuestionData|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see ::getAttributeName()
     * @return string ExtendedAnsweredQuestionData
     */
    public function getAttributeName()
    {
        return 'ExtendedAnsweredQuestionData';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see ::__set_state()
     * @uses ::__set_state()
     * @param array $array the exported values
     * @return \KURZ\VisitNet\PublicService\Arrays\ArrayOfExtendedAnsweredQuestionData
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
