<?php

namespace KURZ\VisitNet\PublicService;

/**
 * Class which returns the class map definition
 * @package
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get()
    {
        return array(
            'ArrayOfAnsweredQuestionData' => '\\KURZ\\VisitNet\\PublicService\\Arrays\\ArrayOfAnsweredQuestionData',
            'AnsweredQuestionData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\AnsweredQuestionData',
            'AnswerData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\AnswerData',
            'ArrayOfTranslationData' => '\\KURZ\\VisitNet\\PublicService\\Arrays\\ArrayOfTranslationData',
            'TranslationData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\TranslationData',
            'QuestionData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\QuestionData',
            'ArrayOfAnswerData' => '\\KURZ\\VisitNet\\PublicService\\Arrays\\ArrayOfAnswerData',
            'ArrayOfExtendedAnsweredQuestionData' => '\\KURZ\\VisitNet\\PublicService\\Arrays\\ArrayOfExtendedAnsweredQuestionData',
            'ExtendedAnsweredQuestionData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\ExtendedAnsweredQuestionData',
            'AnsweredQuestionFreeTextData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\AnsweredQuestionFreeTextData',
            'AnsweredQuestionSingleSelectData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\AnsweredQuestionSingleSelectData',
            'AnsweredQuestionMultiSelectData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\AnsweredQuestionMultiSelectData',
            'ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1' => '\\KURZ\\VisitNet\\PublicService\\Arrays\\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1',
            'KeyValueOfstringArrayOfstringty7Ep6D1' => '\\KURZ\\VisitNet\\PublicService\\Structs\\KeyValueOfstringArrayOfstringty7Ep6D1',
            'ArrayOfstring' => '\\KURZ\\VisitNet\\PublicService\\Arrays\\ArrayOfstring',
            'BinaryFile' => '\\KURZ\\VisitNet\\PublicService\\Structs\\BinaryFile',
            'VisitData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\VisitData',
            'ArrayOfEmployeeData' => '\\KURZ\\VisitNet\\PublicService\\Arrays\\ArrayOfEmployeeData',
            'EmployeeData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\EmployeeData',
            'PersonData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\PersonData',
            'ArrayOfGuestData' => '\\KURZ\\VisitNet\\PublicService\\Arrays\\ArrayOfGuestData',
            'GuestData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GuestData',
            'AutomobileData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\AutomobileData',
            'EnterpriseData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\EnterpriseData',
            'ScheduleData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\ScheduleData',
            'GateData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GateData',
            'ArrayOfVisitorData' => '\\KURZ\\VisitNet\\PublicService\\Arrays\\ArrayOfVisitorData',
            'VisitorData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\VisitorData',
            'DeliveryData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\DeliveryData',
            'VisitRequestData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\VisitRequestData',
            'ArrayOfVisitData' => '\\KURZ\\VisitNet\\PublicService\\Arrays\\ArrayOfVisitData',
            'IsValidVisit' => '\\KURZ\\VisitNet\\PublicService\\Structs\\IsValidVisit',
            'IsValidVisitResponse' => '\\KURZ\\VisitNet\\PublicService\\Structs\\IsValidVisitResponse',
            'IsScheduledExitTimeExceeded' => '\\KURZ\\VisitNet\\PublicService\\Structs\\IsScheduledExitTimeExceeded',
            'IsScheduledExitTimeExceededResponse' => '\\KURZ\\VisitNet\\PublicService\\Structs\\IsScheduledExitTimeExceededResponse',
            'GetVisitDataByCode' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetVisitDataByCode',
            'GetVisitDataByCodeResponse' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetVisitDataByCodeResponse',
            'GetVisitDataByIDCard' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetVisitDataByIDCard',
            'GetVisitDataByIDCardResponse' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetVisitDataByIDCardResponse',
            'GetGuestDataByCode' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetGuestDataByCode',
            'GetGuestDataByCodeResponse' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetGuestDataByCodeResponse',
            'GetDeliveryData' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetDeliveryData',
            'GetDeliveryDataResponse' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetDeliveryDataResponse',
            'SubmitEntryWeight' => '\\KURZ\\VisitNet\\PublicService\\Structs\\SubmitEntryWeight',
            'SubmitEntryWeightResponse' => '\\KURZ\\VisitNet\\PublicService\\Structs\\SubmitEntryWeightResponse',
            'SubmitExitWeight' => '\\KURZ\\VisitNet\\PublicService\\Structs\\SubmitExitWeight',
            'SubmitExitWeightResponse' => '\\KURZ\\VisitNet\\PublicService\\Structs\\SubmitExitWeightResponse',
            'RequestVisit' => '\\KURZ\\VisitNet\\PublicService\\Structs\\RequestVisit',
            'RequestVisitResponse' => '\\KURZ\\VisitNet\\PublicService\\Structs\\RequestVisitResponse',
            'GetWelcomeMonitorRelatedVisits' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetWelcomeMonitorRelatedVisits',
            'GetWelcomeMonitorRelatedVisitsResponse' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetWelcomeMonitorRelatedVisitsResponse',
            'GetWelcomeMonitorRelatedVisitsInMinutes' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetWelcomeMonitorRelatedVisitsInMinutes',
            'GetWelcomeMonitorRelatedVisitsInMinutesResponse' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetWelcomeMonitorRelatedVisitsInMinutesResponse',
            'GetPersonVisits' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetPersonVisits',
            'GetPersonVisitsResponse' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetPersonVisitsResponse',
            'GetDeliveryVisits' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetDeliveryVisits',
            'GetDeliveryVisitsResponse' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetDeliveryVisitsResponse',
            'GetContractorVisits' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetContractorVisits',
            'GetContractorVisitsResponse' => '\\KURZ\\VisitNet\\PublicService\\Structs\\GetContractorVisitsResponse',
            'LicenseFault' => '\\KURZ\\VisitNet\\PublicService\\Structs\\LicenseFault',
            'PublicServiceFault' => '\\KURZ\\VisitNet\\PublicService\\Structs\\PublicServiceFault',
            'TooManyResultsFault' => '\\KURZ\\VisitNet\\PublicService\\Structs\\TooManyResultsFault',
            'PeriodTooLongFault' => '\\KURZ\\VisitNet\\PublicService\\Structs\\PeriodTooLongFault',
        );
    }
}
